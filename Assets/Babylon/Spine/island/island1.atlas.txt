
island1.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
deco/flag L
  rotate: false
  xy: 645, 650
  size: 60, 64
  orig: 60, 64
  offset: 0, 0
  index: -1
deco/flag R
  rotate: false
  xy: 2007, 1970
  size: 34, 76
  orig: 34, 76
  offset: 0, 0
  index: -1
deco/flag_pile L
  rotate: false
  xy: 1696, 1584
  size: 75, 125
  orig: 75, 125
  offset: 0, 0
  index: -1
deco/flag_pile R
  rotate: true
  xy: 699, 939
  size: 55, 128
  orig: 55, 128
  offset: 0, 0
  index: -1
deco/rock1
  rotate: true
  xy: 1949, 1703
  size: 76, 69
  orig: 76, 69
  offset: 0, 0
  index: -1
deco/rock2
  rotate: true
  xy: 497, 256
  size: 53, 50
  orig: 53, 50
  offset: 0, 0
  index: -1
deco/rock3
  rotate: false
  xy: 1773, 1607
  size: 51, 102
  orig: 51, 102
  offset: 0, 0
  index: -1
deco/rock4
  rotate: false
  xy: 1029, 1337
  size: 27, 31
  orig: 27, 31
  offset: 0, 0
  index: -1
deco/rock5
  rotate: true
  xy: 2, 2
  size: 45, 99
  orig: 45, 99
  offset: 0, 0
  index: -1
deco/rock6
  rotate: true
  xy: 294, 23
  size: 24, 22
  orig: 24, 22
  offset: 0, 0
  index: -1
deco/rock7
  rotate: true
  xy: 462, 181
  size: 32, 31
  orig: 32, 31
  offset: 0, 0
  index: -1
deco/rock8
  rotate: false
  xy: 462, 215
  size: 29, 38
  orig: 29, 38
  offset: 0, 0
  index: -1
deco/rock9
  rotate: true
  xy: 318, 39
  size: 53, 91
  orig: 53, 91
  offset: 0, 0
  index: -1
deco/tree c1
  rotate: false
  xy: 1545, 1411
  size: 38, 58
  orig: 38, 58
  offset: 0, 0
  index: -1
deco/tree c10
  rotate: true
  xy: 1239, 1365
  size: 40, 76
  orig: 40, 76
  offset: 0, 0
  index: -1
deco/tree c11
  rotate: false
  xy: 2002, 1640
  size: 44, 61
  orig: 44, 61
  offset: 0, 0
  index: -1
deco/tree c2
  rotate: false
  xy: 945, 1170
  size: 77, 98
  orig: 77, 98
  offset: 0, 0
  index: -1
deco/tree c3
  rotate: false
  xy: 645, 716
  size: 67, 99
  orig: 67, 99
  offset: 0, 0
  index: -1
deco/tree c4
  rotate: true
  xy: 829, 933
  size: 61, 95
  orig: 61, 95
  offset: 0, 0
  index: -1
deco/tree c5
  rotate: false
  xy: 1644, 1410
  size: 47, 56
  orig: 47, 56
  offset: 0, 0
  index: -1
deco/tree c6
  rotate: false
  xy: 512, 814
  size: 95, 119
  orig: 95, 119
  offset: 0, 0
  index: -1
deco/tree c7
  rotate: false
  xy: 2007, 1841
  size: 37, 58
  orig: 37, 58
  offset: 0, 0
  index: -1
deco/tree c8
  rotate: false
  xy: 576, 650
  size: 67, 59
  orig: 67, 59
  offset: 0, 0
  index: -1
deco/tree c9
  rotate: true
  xy: 1829, 1706
  size: 73, 118
  orig: 73, 118
  offset: 0, 0
  index: -1
deco/tree g1
  rotate: false
  xy: 609, 817
  size: 79, 116
  orig: 79, 116
  offset: 0, 0
  index: -1
deco/tree g10
  rotate: true
  xy: 1912, 1635
  size: 66, 88
  orig: 66, 88
  offset: 0, 0
  index: -1
deco/tree g2
  rotate: false
  xy: 1826, 1617
  size: 84, 87
  orig: 84, 87
  offset: 0, 0
  index: -1
deco/tree g3
  rotate: false
  xy: 2007, 1901
  size: 37, 67
  orig: 37, 67
  offset: 0, 0
  index: -1
deco/tree g4
  rotate: false
  xy: 576, 711
  size: 67, 101
  orig: 67, 101
  offset: 0, 0
  index: -1
deco/tree g5
  rotate: true
  xy: 410, 255
  size: 54, 85
  orig: 54, 85
  offset: 0, 0
  index: -1
deco/tree g6
  rotate: false
  xy: 1160, 1365
  size: 77, 89
  orig: 77, 89
  offset: 0, 0
  index: -1
deco/tree g7
  rotate: true
  xy: 945, 1370
  size: 84, 108
  orig: 84, 108
  offset: 0, 0
  index: -1
deco/tree g8
  rotate: false
  xy: 1310, 1410
  size: 67, 82
  orig: 67, 82
  offset: 0, 0
  index: -1
deco/tree g9
  rotate: false
  xy: 1379, 1422
  size: 75, 70
  orig: 75, 70
  offset: 0, 0
  index: -1
deco/tree m1
  rotate: true
  xy: 1317, 1356
  size: 52, 62
  orig: 52, 62
  offset: 0, 0
  index: -1
deco/tree m2
  rotate: false
  xy: 945, 1270
  size: 82, 98
  orig: 82, 98
  offset: 0, 0
  index: -1
deco/tree m3
  rotate: false
  xy: 399, 99
  size: 54, 78
  orig: 54, 78
  offset: 0, 0
  index: -1
deco/tree m4
  rotate: true
  xy: 1055, 1372
  size: 82, 103
  orig: 82, 103
  offset: 0, 0
  index: -1
deco/tree r1
  rotate: true
  xy: 411, 39
  size: 58, 50
  orig: 58, 50
  offset: 0, 0
  index: -1
deco/tree r2
  rotate: false
  xy: 399, 179
  size: 61, 74
  orig: 61, 74
  offset: 0, 0
  index: -1
deco/tree r3
  rotate: false
  xy: 2007, 1786
  size: 36, 53
  orig: 36, 53
  offset: 0, 0
  index: -1
deco/tree t1
  rotate: true
  xy: 1449, 1361
  size: 49, 50
  orig: 49, 50
  offset: 0, 0
  index: -1
deco/tree t2
  rotate: true
  xy: 945, 1004
  size: 69, 78
  orig: 69, 78
  offset: 0, 0
  index: -1
deco/tree t3
  rotate: true
  xy: 237, 2
  size: 45, 55
  orig: 45, 55
  offset: 0, 0
  index: -1
deco/tree t4
  rotate: false
  xy: 945, 1075
  size: 78, 93
  orig: 78, 93
  offset: 0, 0
  index: -1
deco/tree t5
  rotate: false
  xy: 1239, 1407
  size: 69, 85
  orig: 69, 85
  offset: 0, 0
  index: -1
deco/tree t6
  rotate: true
  xy: 1381, 1367
  size: 53, 66
  orig: 53, 66
  offset: 0, 0
  index: -1
house/board
  rotate: false
  xy: 1585, 1411
  size: 57, 55
  orig: 57, 55
  offset: 0, 0
  index: -1
house/board_column
  rotate: true
  xy: 1456, 1412
  size: 57, 87
  orig: 57, 87
  offset: 0, 0
  index: -1
house/house
  rotate: false
  xy: 1232, 1494
  size: 227, 210
  orig: 227, 210
  offset: 0, 0
  index: -1
house/house_column
  rotate: false
  xy: 1588, 1468
  size: 100, 112
  orig: 100, 112
  offset: 0, 0
  index: -1
house/house_pile
  rotate: true
  xy: 103, 5
  size: 42, 132
  orig: 42, 132
  offset: 0, 0
  index: -1
house/house_porch
  rotate: false
  xy: 1461, 1471
  size: 125, 109
  orig: 125, 109
  offset: 0, 0
  index: -1
land1/ground0
  rotate: false
  xy: 410, 642
  size: 100, 291
  orig: 100, 291
  offset: 0, 0
  index: -1
land1/ground1
  rotate: false
  xy: 2, 1515
  size: 942, 531
  orig: 942, 531
  offset: 0, 0
  index: -1
land1/ground2
  rotate: false
  xy: 2, 935
  size: 544, 578
  orig: 544, 578
  offset: 0, 0
  index: -1
land1/ground3
  rotate: false
  xy: 946, 1706
  size: 743, 340
  orig: 743, 340
  offset: 0, 0
  index: -1
land1/ground4
  rotate: true
  xy: 2, 282
  size: 651, 406
  orig: 651, 406
  offset: 0, 0
  index: -1
land1/mountain1
  rotate: true
  xy: 548, 996
  size: 517, 353
  orig: 517, 353
  offset: 0, 0
  index: -1
land1/mountain2
  rotate: false
  xy: 2, 49
  size: 314, 231
  orig: 314, 231
  offset: 0, 0
  index: -1
land1/water
  rotate: false
  xy: 1461, 1582
  size: 233, 122
  orig: 233, 122
  offset: 0, 0
  index: -1
sign
  rotate: false
  xy: 946, 1456
  size: 284, 248
  orig: 284, 248
  offset: 0, 0
  index: -1
sign shadow
  rotate: true
  xy: 318, 94
  size: 186, 79
  orig: 186, 79
  offset: 0, 0
  index: -1
tree
  rotate: false
  xy: 1691, 1781
  size: 314, 265
  orig: 314, 265
  offset: 0, 0
  index: -1
windmill/fence F
  rotate: false
  xy: 1691, 1711
  size: 136, 68
  orig: 136, 68
  offset: 0, 0
  index: -1
windmill/house F
  rotate: false
  xy: 410, 450
  size: 128, 190
  orig: 128, 190
  offset: 0, 0
  index: -1
windmill/house R
  rotate: false
  xy: 410, 311
  size: 127, 137
  orig: 127, 137
  offset: 0, 0
  index: -1
windmill/house fan1
  rotate: false
  xy: 903, 1386
  size: 40, 127
  orig: 40, 127
  offset: 0, 0
  index: -1
windmill/house fan2
  rotate: false
  xy: 903, 1257
  size: 40, 127
  orig: 40, 127
  offset: 0, 0
  index: -1
windmill/house fan3
  rotate: false
  xy: 903, 1128
  size: 40, 127
  orig: 40, 127
  offset: 0, 0
  index: -1
windmill/house fan4
  rotate: false
  xy: 903, 999
  size: 40, 127
  orig: 40, 127
  offset: 0, 0
  index: -1
windmill/house fence R1
  rotate: false
  xy: 1773, 1588
  size: 36, 17
  orig: 36, 17
  offset: 0, 0
  index: -1
windmill/house fence R2
  rotate: false
  xy: 512, 651
  size: 33, 17
  orig: 33, 17
  offset: 0, 0
  index: -1
windmill/house flower
  rotate: false
  xy: 548, 935
  size: 149, 59
  orig: 149, 59
  offset: 0, 0
  index: -1
windmill/house ground
  rotate: true
  xy: 512, 670
  size: 142, 62
  orig: 142, 62
  offset: 0, 0
  index: -1
windmill/metal
  rotate: false
  xy: 1501, 1376
  size: 30, 34
  orig: 30, 34
  offset: 0, 0
  index: -1
