﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HUDManager : BaseServicesBehaviour<HUDManager>
{
    [SerializeField] private TextMeshProUGUI _tmpUserName, _tmpUserId, _tmpGold, _tmpDiamond, _tmpExp;
    [SerializeField] private Image _imgExp;
    [SerializeField] private Button _btnBack;

    private void Start()
    {
        _btnBack.onClick.AddListener(() =>
        {
            NetworkUtil.GI().close();
            SceneManager.LoadScene(0);
        });
    }

    public void SetData(ProfileData profileData)
    {
        _tmpUserName.text = profileData.userName;
        _tmpUserId.text = "UserID : "+profileData.userId;
        _tmpGold.text = profileData.gold.ToString();
        _tmpDiamond.text = profileData.diamond.ToString();
        _tmpExp.text = "6/12";//profileData.exp.ToString();
        _imgExp.fillAmount = (float) 6 / 12;
    }
}
