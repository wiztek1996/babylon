﻿using System;
using UnityEngine;

public class WorldManager : MonoBehaviour
{
    [SerializeField] private GameObject[] _arrGoPointer;
    private Camera _camera;
    private Vector3 _cameraPos;
    float speed = 60.0f;

    private const int  MAX_SIZE_CAM = 10; 
    private const int  MIN_SIZE_CAM = 1; 
    
    private void Awake()
    {
        _camera = FindObjectOfType<Camera>();
    }

    private void FixedUpdate()
    {
        MouseInputCameraControl();
        PanAndZoom();
        foreach (var goPointer in _arrGoPointer)
        {
            goPointer.transform.localPosition = CalCameraPostion(goPointer.transform.localPosition);
        }
    }

    private void PanAndZoom()
    {
        if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
            float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float currentMagnitude = (touchZero.position - touchOne.position).magnitude;
            float difference = currentMagnitude - prevMagnitude;
            if(_camera.orthographicSize <= MAX_SIZE_CAM && _camera.orthographicSize >= MIN_SIZE_CAM )_camera.orthographicSize -= difference * 0.001f;
        }

    }
    
    private void MouseInputCameraControl()
    {
        if (Input.GetMouseButton(0))
        {
            _cameraPos = _camera.transform.localPosition;
            if (Math.Abs(Input.GetAxis("Mouse X")) > 0)
            {
                _camera.transform.localPosition += new Vector3(Input.GetAxisRaw("Mouse X") * Time.deltaTime * speed,
                    Input.GetAxisRaw("Mouse Y") * Time.deltaTime * speed, 0);
            }
        }
    }
    
    private Vector3 CalCameraPostion(Vector3 vtPointer)
    {
        Vector3 vPointer = new Vector3();
        Vector3 vtSub = vtPointer - _cameraPos;
        int rate = 6;
        vPointer.x = (float) -Math.Atan(vtSub.x / rate) * 3;
        vPointer.y = (float) Math.Atan(vtSub.y / rate) * 4 + 0.2f;
        return vPointer;
    }
}