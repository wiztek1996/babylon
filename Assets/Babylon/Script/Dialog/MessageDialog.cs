﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class MessageDialog : BaseServicesBehaviour<MessageDialog>
{
    [SerializeField] private TextMeshProUGUI _tmpMessage;
    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void Show(string content)
    {
        StopAllCoroutines(); 
        gameObject.SetActive(true);
        _tmpMessage.text = content;
        StartCoroutine(DisableCountDown(() => gameObject.SetActive(false), Stats.MESSAGE_COUNT_DOWN));
    }

    private IEnumerator DisableCountDown(Action doSomething,float second)
    {
        yield return new WaitForSeconds(second);
        doSomething?.Invoke();
    }
    
}
