﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseServicesBehaviour<T> : MonoBehaviour where T : BaseServicesBehaviour<T> 
{
    protected virtual void Awake()
    {
        Service.Set(this as T);
    }
    
    protected virtual void OnDestroy()
    {
        Service.Unset<T>();
    }

}
