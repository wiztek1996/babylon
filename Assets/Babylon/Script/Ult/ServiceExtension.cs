﻿

public partial class Service
{
  public static ServerSelect SERVER_SELECT => Get<ServerSelect>();
  public static MessageDialog MESSAGE_DIALOG => Get<MessageDialog>();
  public static LoginManager LOGIN => Get<LoginManager>();
  public static HUDManager HUD => Get<HUDManager>();
}