﻿using UnityEngine;

public static class LoadFromResource
{
    private static GameObject GetGameObj(string path) => Resources.Load(path) as GameObject;

    public static GameObject MAIN_MAP => GetGameObj("MAIN_MAP");
}