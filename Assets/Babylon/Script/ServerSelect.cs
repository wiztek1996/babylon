﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ServerSelect : BaseServicesBehaviour<ServerSelect>
{
    [SerializeField] private GameObject _itemServerPrefab;
    [SerializeField] private Transform _tranContent;

    //Send RequestListServer
    private void Start()
    {
        NetworkUtil.GI().connectToServer(SendData.OnRequestListServers());
    }

    public void InitListServer(List<ServerInfo> lstServer)
    {
        foreach (var server in lstServer)
        {
            GameObject itemSelectServer = Instantiate(_itemServerPrefab, _tranContent);
            itemSelectServer.GetComponentInChildren<TextMeshProUGUI>().text = server.name;
            itemSelectServer.GetComponent<Button>().interactable = server.isUnLock;
            itemSelectServer.GetComponent<Button>().onClick.AddListener(() =>
            {
                Res.IP = server.ip;
                Res.PORT = server.port;
                B.Instance.IsLogin = true;
                NetworkUtil.GI().runConnect();
                SendData.OnCheckUpdateGame();
                SendData.OnCurrentScreen(ScreenData.Login);
                Destroy(gameObject);
            });
        }
    }
}