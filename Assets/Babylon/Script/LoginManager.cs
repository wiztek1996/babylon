﻿using TMPro;
using UniRx;
using UniRx.Extensions;
using UnityEngine;
using UnityEngine.UI;

public class LoginManager : BaseServicesBehaviour<LoginManager>
{
    [SerializeField] private TMP_InputField _tmpUserName, _tmpPassword;
    [SerializeField] private TextMeshProUGUI _tmpInfo;
    [SerializeField] private Button _btnLogin,_btnRegister;
    private string userName;
    private string passWord;
    
  private void Start()
  {
      _btnLogin.onClick.AddListener(ClickToLogin);
    _btnRegister.onClick.AddListener(ClickToRegister);
    LoadSaveLoginData();
  }

  public void SetInfoFromServer(int versionCode , int versionLevel,string hotline)
  {
      _tmpInfo.text = $@"VERSION_CODE :{versionCode}    VERSION_LEVEL : {versionLevel}    HOTLINE : {hotline}";
  }
  
  private void LoadSaveLoginData()
  {
      _tmpUserName.text = PlayerPrefs.GetString(Stats.USERNAME, string.Empty);
      _tmpPassword.text = PlayerPrefs.GetString(Stats.PASSWORD, string.Empty);

  }
  private void ShowMessage(string content) => Service.MESSAGE_DIALOG.Show(content);

  private void CheckInputField()
  {
      userName = _tmpUserName.text.Trim();
      passWord = _tmpPassword.text.Trim();
      if (userName == string.Empty)
      {
          ShowMessage("Tài khoản không được để trống");
          return;
      }
      if (passWord != string.Empty) return;
      ShowMessage("Mật khẩu không được để trống");
  }
  
  private void ClickToRegister()
  {
      CheckInputField();
      SendData.OnRegister(userName,passWord);
  }


  private void ClickToLogin()
  {
      CheckInputField();
      SendData.OnLogin(userName,passWord,LoginType.LOGIN_SIGNIN);
  }

  /// <summary>
  /// Process after login Successfull
  /// </summary>

  public void OnLoginSuccess()
  {
      GameObject gO = Instantiate(LoadFromResource.MAIN_MAP);
      gO.transform.SetAsFirstSibling();
      gO.SetActive(true);
      Destroy(gameObject,1f);
  }
}
