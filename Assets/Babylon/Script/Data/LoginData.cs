﻿public class LoginData
{
    public bool isInboxNew;
    public bool isDiaryNew;
    public bool isNewEvent;
    public byte smsStatus;
    public byte giftStatus;
    public byte statusSms9029;
    public byte statusScratchCard;
    public byte statusInApp;
}