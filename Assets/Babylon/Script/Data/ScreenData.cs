﻿public enum ScreenData
{
    Login = 0,
    FarmScreen = 1,
    ShopScreen = 3,
    StoreScreen,
    MachineScreen = 2,
    GameMiniScreen = 4,
    SelectLand = 5,
    IntelligenceRoom = 6,
    IntelligenceScreen = 7,
    TLMNRoom = 8,
    TLMNScreen = 9,
}