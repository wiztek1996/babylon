﻿public enum LoginType
{
    LOGIN_FACEBOOK = 1,
    LOGIN_GMAIL = 2,
    LOGIN_SIGNIN = 3,
    LOGIN_SMS = 4,
    LOGIN_DIRECTLY = 5
}