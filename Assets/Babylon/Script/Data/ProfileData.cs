﻿public class ProfileData
{
    public int userId;
    public string userName;
    public long gold;
    public int diamond;
    public int exp;
    public byte isGender;
    public byte isHair;
    public byte isEyes;
    public byte isShirt;
    public byte isJean;
    public byte isShoes;
    public byte isTutorial;
}

public class Cloud
{
    public byte pos;
    public bool isUnlock;
}