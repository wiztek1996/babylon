﻿public class CommandProfileSystem : BaseCommand
{
    public CommandProfileSystem(Message msg) : base(msg)
    {
        ProfileData profileData = new ProfileData
        {
            userId = msg.readInt(),
            userName = msg.readUTF(),
            gold = msg.readLong(),
            diamond = msg.readInt(),
            exp = msg.readInt(),
            isGender = msg.readByte(),
            isHair = msg.readByte(),
            isEyes = msg.readByte(),
            isShirt = msg.readByte(),
            isJean = msg.readByte(),
            isShoes = msg.readByte(),
            isTutorial = msg.readByte()
        };
        Service.HUD.SetData(profileData);
    }
}