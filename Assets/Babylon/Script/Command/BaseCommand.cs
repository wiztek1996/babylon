﻿public class BaseCommand
{
    public BaseCommand(Message msg)
    {
    }

    protected void ShowMessage(string content) => Service.MESSAGE_DIALOG.Show(content);
}