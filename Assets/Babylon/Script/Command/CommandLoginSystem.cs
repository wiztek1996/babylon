﻿public class CommandLoginSystem : BaseCommand
{
    public CommandLoginSystem(Message msg) : base(msg)
    {
        ShowMessage("Login Success !");
        Service.LOGIN.OnLoginSuccess();
        B.Instance.LoginData = new LoginData
        {
            //0 disable 1 enable
            isInboxNew = msg.readBoolean(),
             isDiaryNew = msg.readBoolean(),
            isNewEvent = msg.readBoolean(),
            smsStatus = msg.readByte(),
            giftStatus = msg.readByte(),
            statusSms9029 = msg.readByte(),
            statusScratchCard = msg.readByte(),
            statusInApp = msg.readByte(),
        };
    }
}