﻿using UnityEngine;

public class CommandCheckUpdateSystem : BaseCommand
{
    public CommandCheckUpdateSystem(Message msg) : base(msg)
    {
        byte versionCode = msg.readByte();
        byte versionLevel = msg.readByte();
        bool isNewVersion = msg.readBoolean();
        if (isNewVersion) {
            string linkDownload = msg.readUTF();
            ShowMessage("Bạn cần cập nhật phiên bản cho game!");
            Application.OpenURL(linkDownload);
        }
        string hotLine = msg.readUTF();
        Service.LOGIN.SetInfoFromServer(versionCode,versionLevel,hotLine);
    }
}