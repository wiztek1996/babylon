﻿public class TopBase
{     
    public string DisplayName { get; set; }
    public int IdAvatar { get; set; }
    public long Money { get; set; }
    public int Id { get; set; }

    public TopBase(string displayname, int idAvatar, long money, int id)
    {
        this.DisplayName = displayname;
        this.IdAvatar = idAvatar;
        this.Money = money;
        this.Id = id;
    }
}
