﻿public class TyGiaBase
{
    public int MenhGia { get; set; }
    public int Xu { get; set; }
    public int KhuyenMai { get; set; }

    public TyGiaBase(int menhGia, int xu)
    {
        this.MenhGia = menhGia;
        this.Xu = xu;
    }
}
