﻿public class HistoryGiftBase {
    public int ID { get; set; }
    public string Name { get; set; }
    public string Status { get; set; }
    public string time { get; set; }

    public HistoryGiftBase(string name, string status, string time)
    {
        this.Name = name;
        this.Status = status;
        this.time = time;
    }
}
