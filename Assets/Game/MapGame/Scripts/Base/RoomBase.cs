﻿public class RoomBase
{
    public int STT { get; set; }
    public int TenBan { get; set; }
    public int MucCuoc { get; set; }
    public int PlayerCurrent { get; set; }
    public int PlayerMax { get; set; }
    public bool IsLock { get; set; }
}
