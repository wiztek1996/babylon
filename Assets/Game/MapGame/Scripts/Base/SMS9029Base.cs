﻿public class SMS9029Base
{
    public string MoneyThat { get; set; }
    public string Syntax { get; set; }
    public short Port { get; set; }
    public long TienXu { get; set; }

    public SMS9029Base(string moneyThat, string syntax, short port, long tienXu)
    {
        this.MoneyThat = moneyThat;
        this.Syntax = syntax;
        this.Port = port;
        this.TienXu = tienXu;
    }
}
