﻿public class EmailBase : Base
{
    public string noiDung { get; set; }
    public string guitu { get; set; }
    public string guiLuc { get; set; }
    public bool isread { get; set; }
    public int id { get; set; }

    public EmailBase(int id, string guiTu, string guiLuc, string noiDung, int isread)
    {
        this.id = id;
        this.guitu = guitu;
        this.guiLuc = guiLuc;
        this.noiDung = noiDung;
        if(isread == 1)
            this.isread = true;
        else
            this.isread = false;
    }
}
