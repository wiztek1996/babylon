﻿public class EventBase : Base
{
    public int Id { get; set; }
    public string Title { get; set; }
    public bool IsRead { get; set; }

    public EventBase(int id, string title, string content, bool isRead)
    {
        this.Id = id;
        this.Title = title;
        this.Content = content;
        this.IsRead = isRead;
    }
}
