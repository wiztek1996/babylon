﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using TMPro;


public class DialogThongBao : MonoBehaviour
{
    private Action _actionCallback;
    [SerializeField] private Button btnClose, btnDongY;
    [SerializeField] private TextMeshProUGUI txtTitle, txtContent;
    private void Start()
    {
        btnClose.onClick.AddListener(delegate
        {
            Helper.ClickEffect(btnClose.transform);
        });
        btnDongY.onClick.AddListener(delegate
        {
            Helper.ClickEffect(btnDongY.transform);
        });
    }

    private IEnumerator DelaybtnClose()
    {
        btnClose.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.3f);
        btnClose.gameObject.SetActive(true);
    }
    
    public void ShowEnglish(string txt, Action callback = null)
    {
        this.gameObject.SetActive(true);
        txtTitle.text = "NOTIFY";
        // txtTextDongY.text = "OK";
        txtContent.text = txt;
        if (callback != null)
        {
            _actionCallback = callback;
        }
        StartCoroutine(DelaybtnClose());
    }

    public void Show(string txt, Action callback)
    {
        gameObject.SetActive(true);
        txtTitle.text = Res.TITLE_THONG_BAO;
        // txtTextDongY.text = "Đồng Ý";
        txtContent.text = txt;
        if (callback != null)
        {
            _actionCallback = callback;
        }
        StartCoroutine(DelaybtnClose());
    }


    public void Show(string txt)
    {
        gameObject.SetActive(true);
        txtContent.text = txt;
        StartCoroutine(DelaybtnClose());
    }

    public void Hide()
    {
        if (_actionCallback != null)
        {
            _actionCallback();
            _actionCallback = null;
        }

        gameObject.SetActive(false);
    }
    
}