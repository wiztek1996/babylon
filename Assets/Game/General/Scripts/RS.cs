﻿using UnityEngine;


public class RS : MonoBehaviour
{
    public static RS Instance { get { return instance; } }
    private static RS instance;

    public Sprite[] arrAvatar;
    public Sprite[] arrIconSlot;
    public Sprite[] arrSpriteBangThuongBongDa;
    public Sprite[] arrImgBonus;
    private void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
    public Sprite GetAvatarOfMe()
    {
        if (B.Instance.avatarFB != null)
        {
            return B.Instance.avatarFB;
        }
        else
        {
            return arrAvatar[B.Instance.MainInfo.IdAvatar];
        }
    }
    

}
