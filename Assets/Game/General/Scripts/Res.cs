﻿
using UnityEngine;

public class Res
{

    public static string IP = "app.vuontreobabylon.vn";
    public static int PORT = 4606;
    public static string moreGameLink = "";
    public static string linkVote = "";
    public static string linkGame = "";
    public static string linkForum = "https://www.facebook.com/B%E1%BA%AFn-C%C3%A1-Vip-348240265760896";
    public static string TXT_PhoneNumber = "";
    public static string TXT_PhoneNumber2 = "https://www.facebook.com/B%E1%BA%AFn-C%C3%A1-Vip-348240265760896";
    public static string TXT_EmailDoiThuong = "";
    public static string TXT_EmailnapTien = "";
    public static string TXT_Fanpage = "https://www.facebook.com/B%E1%BA%AFn-C%C3%A1-Vip-348240265760896";
    public static string TXT_ID_Fanpage = "195052420972899";
    
    // ---------------------------------------------------------------------------------------------
    public static string TXT_Noti = "Chào Mừng Bạn Đến với Vương Quốc Hũ";
    public static string TXT_Noti_ENGLISH = "Welcome to Vương Quốc Hũ";
    // ---------------------------------------------------------------------------------------------
    public static string LINK_COIN = "";
    public const string TAIGUI = "Tái gửi";
    public const string KHONGTAIGUI = "Không tái gửi";
    
    public const string TAIGUI_ENGLISH = "Re-send";
    public const string KHONGTAIGUI_ENGLISH = "Do not re-send";
    // ---------------------------------------------------------------------------------------------
    public const string TITLE_THONG_BAO = "THÔNG BÁO";
    public const string BTN_DONG_Y = "Đồng ý";
    public const string BTN_HUY = "Hủy";
    public const string BTN_BAT_DAU = "Bắt đầu";
    
    // ---------------------------------------------------------------------------------------------
    public const string TITLE_THONG_BAO_ENGLISH = "NOTIFY";
    public const string BTN_DONG_Y_ENGLISH = "OK";
    public const string BTN_HUY_ENGLISH = "Cancel";
    public const string BTN_BAT_DAU_ENGLISH = "Start";

    //DoiPass
    public static string SMS_DOI_PASS = "";
    public static string DAU_SO_DOI_PASS = "";

    //ACTIVE
    public static string NOTI_ACTIVE = "";
    public static string SMS_ACTIVE = "";
    public static string PHONE_ACTIVE = "";

    //Reg warning
    public static string USERNAME = "";
    public static string PASSWORD = "";
    
    public static string PP_CHOINGAY = "LoginChoingay";
    public static string PP_LOGINBINHTHUONG = "LoginBinhThuong";
    public static string PP_LOGINFB = "LoginFacebook";

    public static string IMEI = "";

    //Provision
    public static string LINK_PROVISION = "";
    public static string WARNING_PROVISION = "Vui lòng đồng ý điều khoản trước khi chơi. Xin Cảm ơn!";
    public static int TYPE_LOGIN = 0;
    public const string TIEN_VIP1 = " Xu";
    public const string TIEN_VIP2 = " Chip";
    
    public const string TIEN_Xu = " Xu";
    public const string TIEN_Chip = " Chip";

    public const string TIEN_VIP1_ENGLISH = " Gold";
    public const string TIEN_VIP2_ENGLISH = " Gold";

    public const float SPEED_CARD = 0.25f;
    public const int MAX_WIDTH_HA_PHOM = 60;
    public const int MAX_HEIGHT_HA_PHOM = 80;

    public const int MAX_WIDTH_MAU_BINH = 70;
    public const int MAX_HEIGHT_MAU_BINH = 89;
    public static readonly Vector3 MAX_SCALE_MAU_BINH = new Vector3(1.46f,1.46f);


    public const int MAX_WIDTH_CARD_PLAYER_0 = 92;
    public const int MAX_HEIGHT_CARD_PLAYER_0 = 116;
    public const float USE_DAO_CU_COUNTDOWN = 5f;

    public const int MAX_WIDTH_CARD_FIRED = 61;

    // ------------------------------------------------------------------------------------------------
    public const string CHECK_INTERNET = "Bạn cần có kết nối mạng để chơi game. Vui lòng bật Wifi/3G";
    public const string CHECK_LOGIN = "Bạn cần đăng nhập trước !!!";
    public const string MAT_KET_NOI = "Mất kết nối !";
    public const string GAME_DANG_PHAT_TRIEN = "Game đang phát triển.";
    public const string DANG_PHAT_TRIEN = " đang phát triển.";
    
    public const string CHECK_INTERNET_ENGLISH = "You need an internet connection to play the game. Please turn on Wifi / 3G";
    public const string CHECK_LOGIN_ENGLISH = "You need to login first !!!";
    public const string MAT_KET_NOI_ENGLISH = "Disconnected !";
    public const string GAME_DANG_PHAT_TRIEN_ENGLISH = "The game is growing.";
    public const string DANG_PHAT_TRIEN_ENGLISH = " is growing.";
    
    // -------------------------------------------------------------------------------------------------
    public const string LOGIN_0 = "Vui lòng đồng ý điều khoản trước khi chơi. Xin cảm ơn.";
    public const string LOGIN_1 = "Bạn chưa nhập tài khoản hoặc mật khẩu.";
    public const string LOGIN_2 = "Lỗi đăng nhập, tài khoản hoặc mật khẩu không đúng.";
    public const string LOGIN_3 = "Tài khoản phải > 6 ký tự.";
    public const string LOGIN_4 = "Tên tài khoản chứa ký tự không hợp lệ. Tài khoản không được chứa ký tự đặc biệt.";

    public const string LOGIN_1_ENGLISH = "You have not entered an account or password. ";
    public const string LOGIN_3_ENGLISH = "Account must be> 6 characters. ";
    public const string LOGIN_4_ENGLISH = "The account name contains invalid characters. The account cannot contain special characters. ";

    // ---------------------------------------------------------------------------------------------------
    public const string REGISTER_1 = "Mật khẩu không hợp lệ. Mật khẩu phải > 6 ký tự và có cả chữ và số.";
    public const string REGISTER_2 = "Mật khẩu phải > 6 ký tự, bao gồm cả chữ và số.";
    public const string REGISTER_3 = "Hai mật khẩu không khớp nhau. ";
    public const string REGISTER_4 = "Tên tài khoản chứa ký tự không hợp lệ.";
    public const string REGISTER_5 = "Đăng ký tài khoản thất bại, tài khoản đã có người sử dụng.";
    
    public const string REGISTER_1_ENGLISH = "Invalid password. Password must be> 6 characters and include both letters and numbers.";
    public const string REGISTER_2_ENGLISH = "Password must be> 6 characters, including letters and numbers.";
    public const string REGISTER_3_ENGLISH = "The two passwords do not match. ";
    public const string REGISTER_4_ENGLISH = "The account name contains invalid characters.";
    public const string REGISTER_5_ENGLISH = "Account registration failed, the account already has users.";

    // ----------------------------------------------------------------------------------------------------
    public const string CHAT_ADMIN = "Bạn phải nhập nội dung.";
    public const string CHANGE_NAME_0 = "Nhập vào tên mới.";
    public const string CHANGE_NAME_1 = "Tên không được chứa ký tự đặc biệt.";
    public const string CHANGE_NAME_2 = "Tên phải nhiều hơn 4 và ít hơn 20.";
    public const string CHANGE_NAME_3 = "Bạn chỉ được đổi tên hiển thị một lần.";
    
    public const string CHAT_ADMIN_ENGLISH = "You must enter content.";
    public const string CHANGE_NAME_0_ENGLISH = "Enter the new name.";
    public const string CHANGE_NAME_1_ENGLISH = "Names cannot contain special characters.";
    public const string CHANGE_NAME_2_ENGLISH = "Names must be more than 4 and less than 20.";
    public const string CHANGE_NAME_3_ENGLISH = "You can only change the display name once.";

    // -----------------------------------------------------------------------------------------------------
    public const string CHANGE_PASSWORD_0 = "Bạn hãy nhập đủ thông tin.";
    public const string CHANGE_PASSWORD_1 = "Hai mật khẩu mới không khớp.";
    
    public const string CHANGE_PASSWORD_0_ENGLISH = "Please enter enough information.";
    public const string CHANGE_PASSWORD_1_ENGLISH = "Two new passwords do not match.";

    // ------------------------------------------------------------------------------------------------------
    public const string NAP_CARD = "Mã thẻ hoặc Mã seri không hợp lệ.";

    public const string NAP_CARD_ENGLISH = "Invalid card or serial code.";
    
    // -------------------------------------------------------------------------------------------------------
    public const string CHUYEN_XU_0 = "Số Xu chuyển phải lớn hơn 500K.";
    public const string CHUYEN_XU_1 = "Bạn phải nạp tiền ít nhất một lần mới có thể chuyển Xu.";
    public const string CHUYEN_XU_2 = "Bạn chưa nhập UserId người nhận.";
    public const string CHUYEN_XU_3 = "Xu của bạn không đủ để chuyển.";

    public const string AVATAR = "Cập nhật avatar thành công.";

    public const string TAO_BAN_0 = "Số người phải lớn hơn 1 và nhỏ hơn 5";

    // -------------------------------------------------------------------------------------------------------
    public const string JOIN_ROOM = "Bạn không còn tiền. bạn có muốn nạp tiền không?";
    public const string JOIN_ROOM_1 = "Bàn đang chơi, xin chờ.";
    public const string JOIN_ROOM_2 = "Bạn không đủ tiền để chơi bàn cược này.";

    public const string JOIN_ROOM_ENGLISH = "You have no money. Do you want to recharge money?";
    public const string JOIN_ROOM_1_ENGLISH = "The table is playing, please wait.";
    public const string JOIN_ROOM_2_ENGLISH = "You do not have enough money to play this bet.";

    // -------------------------------------------------------------------------------------------------------
    public const string TOI_BAN = "Bạn chưa nhập tên bàn.";
    public const string TITLE_THIEU_MONEY = "Bạn không đủ tiền để tham gia chơi";
    public const string CHUA_CHON = "Bạn chưa chọn cửa đặt.";
    public const string KHONG_DANH_DUOC = "Không đánh được!";
    public const string CHUA_SAN_SANG = "Chưa đủ người chơi hoặc người chơi chưa sẵn sàng";
    public const string FULL_PLAYER = "Bàn chơi đã đầy";
    
    public const string TOI_BAN_ENGLISH = "You did not enter the table name";
    public const string KHONG_DANH_DUOC_ENGLISH = "Can't hit!";
    public const string CHUA_SAN_SANG_ENGLISH = "Not enough players or players are not ready";
    public const string FULL_PLAYER_ENGLISH = "The table is full";

    // -----------------------------------------------------------------------------------------------------
    public const string CHON_BAI = "Chưa chọn bài!";
    public const string ROI_BAN = "Bạn đã đăng ký rời bàn thành công !";
    public const string ROI_BAN_2 = "Bạn có muốn hủy đăng ký rời bàn không?";
    public const string ROI_BAN_3 = "        Bạn có muốn rời bàn không?";
    
    public const string CHON_BAI_ENGLISH = "No card selected";
    public const string ROI_BAN_ENGLISH = "You have successfully signed up to leave the table!";
    public const string ROI_BAN_2_ENGLISH = "Do you want to unsubscribe from the table?";
    public const string ROI_BAN_3_ENGLISH = "        Do you want to leave the table?";
    
    // ----------------------------------------------------------------------------------------------------
    public const string TX_1 = "Không được đặt cả hai cửa trong 1 ván.";
    public const string TX_2 = "Vui lòng chọn mức cược > 0";
    public const string TX_3 = "Bạn không đủ tiền để tham gia đặt cược";
    public const string TX_4 = "Không có dữ liệu phiên.";
    public const string TX_5 = "Tiền cược tối thiểu 1000. Vui lòng đặt lại.";

    public const string TX_1_ENGLISH = "Do not place both doors in a game.";
    public const string TX_2_ENGLISH = "Please select a bet level> 0";
    public const string TX_3_ENGLISH = "You do not have enough money to place a bet";
    public const string TX_4_ENGLISH = "No session data.";

    //XiTo ------------------------------------------------------------------------------------------------
    public const string TXT_FOLD = "Bỏ";
    public const string TXT_CHECK = "Xem";
    public const string TXT_CALL = "Theo";
    public const string TXT_CALL_ANY = "Tố";
    //public const string TXT_RAISE = "Tố";
    public const string TXT_ALLIN = "Tất tay";

    public const int AC_XEMBAI = 0;
    public const int AC_BOLUOT = 1;
    public const int AC_THEO = 2;
    public const int AC_UPBO = 3;
    public const int AC_TO = 4;

    public const string BC_CHON_BAI = "Chọn 1 trong 2 quân bài để mở: ";
    
    //Ba Cay ---------------------------------------------------------------------------------------------
    // VietNamese
    public const string BC_XIN_CHO = "Xin chờ";
    public const string BC_DAT_CUOC = "Đặt cược";
    public const string BC_NAN_BAI = "Nặn bài";
    
    // English
    public const string BC_XIN_CHO_ENGLISH = "Please wait";
    public const string BC_DAT_CUOC_ENGLISH = "Place a bet";
    public const string BC_NAN_BAI_ENGLISH = "Play cards";
    
    //Xoc Dia --------------------------------------------------------------------------------------------
    // VietNamese
    public const string XD_NHA_CAI_XOC = "Nhà cái bắt đầu xóc";
    public const string XD_DAT_CUOC = "Đặt cược";
    public const string XD_NHA_CAI_DUNG_CUOC = "Nhà cái dừng cược";
    public const string XD_HET_TIME_DAT_CUOC = "Chưa đến thời gian đặt cược !";
    public const string XD_LAM_CAI = "Cần ít nhất 2 người chơi để làm cái.";
    public const string XD_NGUNG_NHAN_CUOC = "Nhà Cái Ngừng Nhận Cược";
    public const string XD_CHO_VAN_MOI = "Chờ bắt đầu ván mới";
    public const string XD_MO_BAT = "Mở Bát";
    public const string XD_HUY_CUA_CHAN = "Nhà cái hủy của chẵn";
    public const string XD_HUY_CUA_LE = "Nhà cái hủy của lẻ";
    
    // English
    public const string XD_NHA_CAI_XOC_ENGLISH = "The dealer starts bumping";
    public const string XD_DAT_CUOC_ENGLISH = "Place a bet";
    public const string XD_NHA_CAI_DUNG_CUOC_ENGLISH = "The dealer stops betting";
    public const string XD_HET_TIME_DAT_CUOC_ENGLISH = "Yet to come time to place bets!";
    public const string XD_LAM_CAI_ENGLISH = "At least 2 players are needed to make the bet";
    public const string XD_NGUNG_NHAN_CUOC_ENGLISH = "The House stops accepting bets";
    public const string XD_CHO_VAN_MOI_ENGLISH = "Waiting for the start of the new game";
    public const string XD_MO_BAT_ENGLISH = "Open Bowl";
    public const string XD_HUY_CUA_CHAN_ENGLISH = "The dealer cancels even";
    public const string XD_HUY_CUA_LE_ENGLISH = "House of cancellation of odd";
    
    // Poker Mini------------------------------------------------------------------------------------------
    // VietNamese
    public const string PK_LICH_SU = "Bạn không có lịch sử chơi Poker Mini";
    public const string NOTIFY_PokerMN = "Vui lòng chờ lượt quay kết thúc.";
    
    // English
    public const string PK_LICH_SU_ENGLISH = "You have no history of playing Mini Poker";
    
    
    // Bầu cua ------------------------------------------------------------------------------------------
    // VietNamese
    public const string BC_LICH_SU = "Bạn chưa có lịch sử chơi game Bầu Cua";
    public const string BC_MOI_CHOI = "Xin Mời Đặt Cược";
    public const string BC_DAT_CUOC_SUCCESS = "Đặt Cược Thành Công!";
    public const string BC_CACH_CHOI = "Cách chơi\n" +
                                       "•\tBước 1: chọn bàn và mệnh giá chơi. Người chơi có thể chơi nhiều cửa tại một thời điểm.\n" +
                                       "•\tBước 2: chọn cửa đặt\n" +
                                       "•\tBước 3: click button xác nhận để gửi giao dịch. Sau khi gửi xác nhận người chơi có thể tiếp tục đặt cửa. Sau khi đặt thêm click button xác nhận để gửi.\n" +
                                       "•\tBước 4: chờ kết quả và nhận vàng. Trong thời gian chờ, người chơi không thể chuyển bàn.\n" +
                                       "Trả xu\n" +
                                       "Hệ thống mở bát, dựa vào kết quả 3 mặt của 3 xúc xắc để phân định thắng thua. Người chơi đặt cửa có kết quả giống với mặt xúc xắc sẽ thắng:\n" +
                                       "•\tXúc xắc về một biểu tượng: người chơi ăn được một lần tổng đặt \n" +
                                       "•\tXúc xắc về hai biểu tượng: người chơi ăn được hai lần tổng đặt\n" +
                                       "•\tXúc xắc về ba biểu tượng: người chơi ăn được ba lần tổng đặt\n" +
                                       "•\tĐặc biệt người chơi có cơ hội nhận được 9 lần tổng đặt nếu giá trị nhân về x3 và kết quả ba biểu tượng.\n" +
                                       "luật X2,X3:  Trước khi gieo xúc xắc hệ thống sẽ chọn ngẫu nhiên giá trị x1,x2,x3 cho một cửa bất kì. Người chơi chọn đúng cửa được nhân lên theo số mặt xúc xắc trả về.\n";
    
    // English
    public const string BC_LICH_SU_ENGLISH = "You have no history of playing Bau Cua game";
    
    // Cập nhật ------------------------------------------------------------------------------------------
    // VietNamese
    public const string CNH_TTCN = "Cập nhật thông tin cá nhân thành công";
    public const string CNH_TKANDMK = "Tài khoản hoặc mật khẩu không đúng";
    public const string CNH_DAILY = "Bạn là Đại Lý";
    public const string CNH_THAY_DOI_MAT_KHAU_1 = "Bạn đã đổi mật khẩu thành công !";
    public const string CNH_THAY_DOI_MAT_KHAU_2 = "Bạn đã đổi mật khẩu thất bại !";
    
    // English
    public const string CNH_TTCN_ENGLISH = "Update personal information successfully";
    public const string CNH_TKANDMK_ENGLISH = "The account or password is incorrect";
    public const string CNH_DAILY_ENGLISH = "You are agency";
    public const string CNH_THAY_DOI_MAT_KHAU_1_ENGLISH = "You have successfully changed your password !";
    public const string CNH_THAY_DOI_MAT_KHAU_2_ENGLISH = "You have failed to change the password !";

    // TITLE
    public const string TITLE_1 = "Trong khi chơi Mậu Binh không thể chơi Poker Mini.";
    public const string TITLE_2 = "Không được để trống kí tự khi gửi";
    public const string TITLE_3 = "Nội dung không được phép quá 100 kí tự";
    public const string TITLE_HD_TAIXIU = "HƯỚNG DẪN";
    public const string TITLE_HD_BAUCUA = "HƯỚNG DẪN";
    
    // Chat Admin
    public const string CHUA_NHAP_KI_TU = "Bạn phải nhập nội dung !";
}
