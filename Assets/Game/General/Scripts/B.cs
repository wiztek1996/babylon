﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class B
{
    B()
    {
        MainInfo = new MainInfo();
    }

    public static B Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new B();
            }
            return instance;
        }      
    }
    protected static B instance;
    public UnityEngine.Sprite avatarFB;

    public MainInfo MainInfo { get; set; }
    public LoginData LoginData { get; set; }


    public bool IsLogin; // True login ,false Select server

}
