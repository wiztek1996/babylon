﻿using UnityEngine;
public class C
{
    #region PLAYER PREF
    public const string PP_USERNAME = "username";
    public const string PP_PASSWORD = "password";
    public const string PP_TYPE_LOGIN = "typelogin";
    public const string PP_SAVE_ACCOUNT = "saveAccount";
    public const string PP_MUSIC = "music";
    public const string PP_SOUND = "sound";
    public const string PP_VIBRATE = "vibrate";
    public const string PP_RECEIVED_INVITE = "receivedInvite";
    public const string PP_READY = "ready";
    public const string PP_IDROOM_TCT = "idRoomTCT";
    public const string PP_NUMLINE_TCT = "numLineTCT";
    public const string PP_TOKEN_FIREBASE = "tokenfirebase";
    public const string PP_SAVE_LEVEL = "saveLevel";
    public const string PP_GOLD_VALUE = "goldValue";
    public const string PP_SAVE_THUOCNO = "thuocno";
    public const string PP_SAVE_MONEY = "savemoney";
    public const string PP_LANGUAGE = "ngonngu";
    public const string PP_TOKEN_FB = "token"; 
    
    public const string PP_PHUONGTHUCLOGIN = "PlayLogin";

    #endregion

    public const int LENGTH_MIN_USERNAME = 6;
    public const int LENGTH_MIN_PASWORD = 7;
    public const int LENGTH_MIN_DISPLAYNAME = 5;

    public const int TARGET_FRAME = 60;
    internal const long ZERO_LONG = 0L;
    
    public static TextureFormat TEXTURE_FORMAT_DOWNLOADED_IMG = TextureFormat.RGBA32;

    public const int roomIdFree = 1;
    public const int roomId = 2;
    
    //Type Login
    public const byte L_FACEBOOK = 1; 
    public const byte L_LOGIN = 4;
    public const byte L_PLAYNOW = 2;
    public const byte L_TU_DANG_KY = 1;
    public const byte L_KHONG_DANG_KY = 0;

    //Item tren mat nguoi choi trong phong dau  //CMD.CMD_LIST_ITEM

    public const string TBL_PASS = "khoa";

    //sort table
    public const int S_TEN_BAN = 1;
    public const int S_MUC_CUOC = 2;
    public const int S_NGUOI_CHOI = 3;
    
    public const byte TYPE_FREE = 1;
    public const byte TYPE_VIP = 2;

    //joint room
    public const int R_JOIN_ROOM = -1;
    public const int R_TOI_BAN = 0;

    public const float TIME_TAT_CHAT = 3F;
    public const float TIME_HIEN_TIEN_BAY = 3.25F;
    public const float TIME_HIEN_RESULT = 4F;
    public const float TIME_BO_LUOT = 2.5F;
    public const float TIME_EFFECT_WIN = 6F;
    public const float TIME_ACTION = 2.2F;
    public const float TIME_VONG_XOAY = 10F;
    public const float TIME_WAIT_HA_BAI = 0F;

    //image win
    public const int THANG = 0;
    public const int THANG_TRANG = 1;
    public const int CONG = 2;
    public const int MOM = 3;
    public const int LUNG = 4;
    public const int U = 5;

    public const string RS_THANG = "hu_thang";
    public const string RS_THANGTRANG = "thangtrang";
    public const string RS_MOM = "mom";
    public const string RS_CONG = "cong";
    public const string RS_LUNG = "lung";
    public const string RS_U = "phomu";
    //public const int BA = 5;
    //public const int BET = 6;  

    public const int TRUE = 1;
    public const int FALSE = 0;
    
    public const string VIET_NAM = "Vietnamese";
    public const string ENG_LISH = "English";
}
