﻿public class GameInfo
{
    public int IdIconGame { get; set; }
    public int GameId { get; set; }
    public bool IsPublic { get; set; }
    public bool IsSlot { get; set; }

    public GameInfo(int idIconGame, int gameId, bool isPublic, bool isSlot)
    {
        this.IdIconGame = idIconGame;
        this.GameId = gameId;
        this.IsPublic = isPublic;
        this.IsSlot = isSlot;
    }
}
