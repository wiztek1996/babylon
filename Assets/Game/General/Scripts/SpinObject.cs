﻿using UnityEngine;

public class SpinObject : MonoBehaviour
{
    [SerializeField]
    private float speed = 300F;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward * Time.deltaTime * speed);
    }
}
