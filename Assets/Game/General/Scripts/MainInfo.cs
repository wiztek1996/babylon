﻿public class MainInfo
{
    public string NickName { get; set; }
    public long UserID { get; set; }
    public string Name { get; set; }
    public string PhoneNumber { get; set; }
    public long Money { get; set; }// Tien Vip

    public string DisplayName { get; set; }
    public sbyte IsVIP { get; set; }
    // ----- new
    public string SoLanThang { get; set; }
    public string SoLanThua { get; set; }
    public long SoTienMax { get; set; }
    public int SoGDThanhCong { get; set; }
    
    public string dangNhapGanNhat { get; set; }
    public bool IsAutoOutTable { get; set; }
    /**
	 * check xem người chơi có đang chơi trong ván ko 
	 */
    public bool IsPlayingUser { get; set; }

    public long Exp { get; set; }
    public long Score_vip { get; set; }
    public long Total_money_charging { get; set; }
    public long Total_time_play { get; set; }

    public string Link_Avatar { get; set; }
    public int IdAvatar { get; set; }
    public byte Level { get; set; }
    
    public byte LevelVip { get; set; }
    
    public string Created_at { get; set; } // được tạo tại
    
    public string Ip { get; set; }
    
    public long Money_chip { get; set; }
    
    public string Email { get; set; }
    
    public string Log_out_time { get; set; }
    public bool isAgency { get; set; }

    public byte GioiTinh
    {
        get; set;
    }
}
