﻿using UnityEngine;

public class SpinAroundObject : MonoBehaviour
{
    [SerializeField]
    private float speed = 300F;
    public Transform targetCenter;

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(targetCenter.position, Vector3.forward, speed * Time.deltaTime);
    }
}
