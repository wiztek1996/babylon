﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class SD_Toast : MonoBehaviour
{
    private RectTransform objTran;
    public Text txtContent;
    public GameObject obj;
    public Vector3 currentTranform;
    public SD_Toast Instance;

    private void Awake()
    {
        Instance = this;
        gameObject.SetActive(false);
        objTran = obj.GetComponent<RectTransform>();
        currentTranform = objTran.position;
    }
    
    public void ShowToast(string message, int valueY = 80, float time = 1.2f, float timeDestroy = 1.6f)
    {
        txtContent.text = message;
        gameObject.SetActive(true);
        obj.transform.localPosition = new Vector2(0, -100);
        obj.transform.localScale = new Vector3(1.1f,1.1f);
        obj.transform.DOLocalMoveY(valueY, time);
        obj.transform.DOScale(new Vector3(0.85f, 0.85f), time);
        StartCoroutine(AgentUnity.DisableObject(gameObject, timeDestroy));
    }
}