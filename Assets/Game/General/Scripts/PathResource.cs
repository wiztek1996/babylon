﻿using UnityEngine;
using UnityEngine.UI;


public class PathResource
{
    /// <summary>
    /// LoadPrefab
    /// </summary>

    #region Common

    // ------------ Prefab ------------ //
    public const string ResoureManager = "Assets/Slot/Resource/common/ResoureManager.prefab";
    public const string PopupLoading = "Assets/Slot/Resource/common/PopupLoading.prefab";
    
    // ------------ Sprite ------------ //
    public const string Internet = "Assets/Slot/Resource/common/Sprite/internet/G";
    public const string LineS1 = "Assets/Slot/Resource/common/Sprite/LineS1";
    public const string LineS2 = "Assets/Slot/Resource/common/Sprite/LineS2";
    public const string LineS3 = "Assets/Slot/Resource/common/Sprite/LineS3";
    
    #endregion

    #region Game

    // ------------ Prefab ------------ //
    public const string GameManager = "Assets/Slot/Resource/game/Prefab/GameManager.prefab";

    // Game Xóc Đĩa
    public const string PrefabTrang = "Assets/Slot/Resource/game/Prefab/XocDia/PrefabTrang.prefab";
    public const string prefabDo = "Assets/Slot/Resource/game/Prefab/XocDia/prefabDo.prefab";

    public const string chipCuoc = "Assets/Slot/Resource/game/Prefab/XocDia/chipCuoc.prefab";

    // Game Tiến Lên Miền Nam
    public const string TLMN = "Assets/Slot/Resource/game/Prefab/TLMN.prefab";

    public const string TLMNSL = "Assets/Slot/Resource/game/Prefab/TLMNSolo.prefab";

    // Game Phỏm
    public const string Phom = "Assets/Slot/Resource/game/Prefab/Phom.prefab";

    // Game Xóc Đĩa
    public const string XocDia = "Assets/Slot/Resource/game/Prefab/XocDia.prefab";

    // Game Mậu Binh
    public const string MauBinh = "Assets/Slot/Resource/game/Prefab/MauBinh.prefab";

    // Game Sâm
    public const string Sam = "Assets/Slot/Resource/game/Prefab/Sam.prefab";
    
    public const string ThrowItem = "Assets/Slot/Resource/game/Prefab/ThrowItem.prefab";
    public const string Poke = "Assets/Slot/Resource/game/Prefab/Poke.prefab";
    public const string EmojiAnimation = "Assets/Slot/Resource/game/Prefab/EmojiAnimation.prefab";
    public const string EffectU = "Assets/Slot/Resource/game/Prefab/EffectU.prefab";
    public const string EffectThangTrang = "Assets/Slot/Resource/game/Prefab/EffectThangTrang.prefab";
    

    //---- Sprite ----//
    // Gift
    public const string tien = "Assets/Slot/Resource/game/Prefab/Gift/tien.prefab";
    public const string dam = "Assets/Slot/Resource/game/Prefab/Gift/dam.prefab";
    public const string huong = "Assets/Slot/Resource/game/Prefab/Gift/huong.prefab";
    public const string hoa = "Assets/Slot/Resource/game/Prefab/Gift/hoa.prefab";
    public const string bom = "Assets/Slot/Resource/game/Prefab/Gift/bom.prefab";
    public const string bua = "Assets/Slot/Resource/game/Prefab/Gift/bua.prefab";
    
    public const string tgOff = "Assets/Slot/Resource/game/Sprite/tgOff.png";
    public const string tgOn = "Assets/Slot/Resource/game/Sprite/tgOn.png";

    public const string btnOff = "Assets/Slot/Resource/game/Sprite/btnOff.png";
    public const string btnOn = "Assets/Slot/Resource/game/Sprite/btnOn.png";
    
    // Special - Đặc biệt
    public const string BaDoiThong = "Assets/Slot/Resource/game/Sprite/Special/BaDoiThong.png";
    public const string BonDoiThong = "Assets/Slot/Resource/game/Sprite/Special/BonDoiThong.png";
    public const string AnChotHa = "Assets/Slot/Resource/game/Sprite/Special/AnChotHa.png";
    public const string TuQuy = "Assets/Slot/Resource/game/Sprite/Special/TuQuy.png";
    public const string Cong = "Assets/Slot/Resource/game/Sprite/Special/Cong.png";
    public const string Lung = "Assets/Slot/Resource/game/Sprite/Special/Lung.png";
    public const string Mom = "Assets/Slot/Resource/game/Sprite/Special/Mom.png";
    
    #endregion


    #region GameMini

    // Game Bầu Cua
    public const string bc_x2 = "Assets/Slot/Resource/gamemini/Sprite/bc_x2.png";
    public const string bc_x3 = "Assets/Slot/Resource/gamemini/Sprite/bc_x3.png";
    public const string bc_x4 = "Assets/Slot/Resource/gamemini/Sprite/bc_x4.png";
    public const string bc_x6 = "Assets/Slot/Resource/gamemini/Sprite/bc_x6.png";
    public const string bc_x9 = "Assets/Slot/Resource/gamemini/Sprite/bc_x9.png";
    
    // PokerMini
    public const string pkmn_type = "Assets/Slot/Resource/gamemini/Sprite/pkmn_type";
    
    public const string pkmn_MauThau = "Assets/Slot/Resource/gamemini/Sprite/pkmn_type1.png";
    public const string pkmn_Doi = "Assets/Slot/Resource/gamemini/Sprite/pkmn_type2.png";
    public const string pkmn_Thu = "Assets/Slot/Resource/gamemini/Sprite/pkmn_type3.png";
    public const string pkmn_SamCo = "Assets/Slot/Resource/gamemini/Sprite/pkmn_type4.png";
    public const string pkmn_Sanh = "Assets/Slot/Resource/gamemini/Sprite/pkmn_type5.png";
    public const string pkmn_Thung = "Assets/Slot/Resource/gamemini/Sprite/pkmn_type6.png";
    public const string pkmn_CuLu = "Assets/Slot/Resource/gamemini/Sprite/pkmn_type7.png";
    public const string pkmn_TuQuy = "Assets/Slot/Resource/gamemini/Sprite/pkmn_type8.png";
    public const string pkmn_ThungPhaSanh = "Assets/Slot/Resource/gamemini/Sprite/pkmn_type9.png";
    
    public const string bc_dice_ = "bc_dice_";

    // ------------ Prefab ------------ //
    public const string GameMiniController = "Assets/Slot/Resource/gamemini/Prefab/GameMiniController.prefab";

    // Game PokerMini
    public const string DialogHuongDanGameMini = "Assets/Slot/Resource/gamemini/Prefab/DialogHuongDanGameMini.prefab";
    public const string panelPokerMini = "Assets/Slot/Resource/gamemini/Prefab/panelPokerMini.prefab";
    public const string DialogGuidePokerMini = "Assets/Slot/Resource/gamemini/Prefab/DialogGuidePokerMini.prefab";
    public const string DialogLichSuPK = "Assets/Slot/Resource/gamemini/Prefab/DialogLichSuPK.prefab";
    public const string ItemGuidePkM = "Assets/Slot/Resource/gamemini/Prefab/ItemGuidePkM.prefab";

    public const string ItemLogPKMe = "Assets/Slot/Resource/gamemini/Prefab/ItemLogPKMe.prefab";

    // Game Bầu Cua
    public const string ItemLogBC = "Assets/Slot/Resource/gamemini/Prefab/ItemLogBC.prefab";
    public const string DialogLichSuBC = "Assets/Slot/Resource/gamemini/Prefab/DialogLichSuBC.prefab";
    public const string panelBauCua = "Assets/Slot/Resource/gamemini/Prefab/panelBauCua.prefab";

    public const string ItemLogBCMe = "Assets/Slot/Resource/gamemini/Prefab/ItemLogBCMe.prefab";

    // Game Tài Xỉu
    public const string DialogLogSessionTX = "Assets/Slot/Resource/gamemini/Prefab/DialogLogSessionTX.prefab";
    public const string ItemTxtChat = "Assets/Slot/Resource/gamemini/Prefab/ItemTxtChat.prefab";
    public const string ItemLogTaiXiuMe = "Assets/Slot/Resource/gamemini/Prefab/ItemLogTaiXiuMe.prefab";
    public const string ChatTaiXiu = "Assets/Slot/Resource/gamemini/Prefab/ChatTaiXiu.prefab";
    public const string ChatTaiXiuReal = "Assets/Slot/Resource/gamemini/Prefab/ChatTaiXiuReal.prefab";
    public const string DialogSoiCauTaiXiu = "Assets/Slot/Resource/gamemini/Prefab/DialogSoiCauTaiXiu.prefab";
    public const string ItemTaiXiu = "Assets/Slot/Resource/gamemini/Prefab/ItemTaiXiu.prefab";
    public const string DialogLichSuTX = "Assets/Slot/Resource/gamemini/Prefab/DialogLichSuTX.prefab";
    public const string panelTaiXiu = "Assets/Slot/Resource/gamemini/Prefab/panelTaiXiu.prefab";
    public const string ItemLogSession = "Assets/Slot/Resource/gamemini/Prefab/ItemLogSession.prefab";

    // ------------ Sprite ------------ //
    public const string imgNgangTrang = "Assets/Slot/Resource/gamemini/Sprite/imgNgangTrang.png";
    public const string imgNgangLS = "Assets/Slot/Resource/gamemini/Sprite/imgNgangLS.png";
    public const string imgIconTai = "Assets/Slot/Resource/gamemini/Sprite/imgTai.png";
    public const string imgIconXiu = "Assets/Slot/Resource/gamemini/Sprite/imgXiu.png";

    #endregion


    #region Lobby

    // ------------ Prefab ------------ //
    public const string NetworkController = "Assets/Slot/Resource/lobby/NetworkController.prefab";
    public const string LobbyNew = "Assets/Slot/Resource/lobby/LobbyNew.prefab";
    public const string RoomInfo = "Assets/Slot/Resource/lobby/Prefab/RoomInfo.prefab";

    public const string IconBongDa = "Assets/Slot/Resource/lobby/Prefab/IconBongDa.prefab";
    public const string IconTienCa = "Assets/Slot/Resource/lobby/Prefab/IconTienCa.prefab";
    public const string IconTLMN = "Assets/Slot/Resource/lobby/Prefab/IconTLMN.prefab";
    public const string IconTLMNsolo = "Assets/Slot/Resource/lobby/Prefab/IconTLMNsolo.prefab";
    public const string IconPhom = "Assets/Slot/Resource/lobby/Prefab/IconPhom.prefab";
    public const string IconSam = "Assets/Slot/Resource/lobby/Prefab/IconSam.prefab";
    public const string IconMauBinh = "Assets/Slot/Resource/lobby/Prefab/IconMauBinh.prefab";
    public const string IconXocDia = "Assets/Slot/Resource/lobby/Prefab/IconXocDia.prefab";
    public const string IconPokerMini = "Assets/Slot/Resource/lobby/Prefab/IconPokerMini.prefab";
    public const string IconTaiXiu = "Assets/Slot/Resource/lobby/Prefab/IconTaiXiu.prefab";
    public const string IconBauCua = "Assets/Slot/Resource/lobby/Prefab/IconBauCua.prefab";
    
    public const string IconBaCay = "Assets/Slot/Resource/lobby/Prefab/IconBaCay.prefab";
    public const string IconChan = "Assets/Slot/Resource/lobby/Prefab/IconChan.prefab";
    public const string IconLieng = "Assets/Slot/Resource/lobby/Prefab/IconLieng.prefab";
    public const string IconPoker = "Assets/Slot/Resource/lobby/Prefab/IconPoker.prefab";
    public const string IconXiTo = "Assets/Slot/Resource/lobby/Prefab/IconXiTo.prefab";
    
    public const string ItemChatLobby = "Assets/Slot/Resource/lobby/Prefab/ItemChatLobby.prefab";
    public const string VongQuayMayMan = "Assets/Slot/Resource/lobby/Prefab/VongQuayMayMan.prefab";
    public const string ChatLobby = "Assets/Slot/Resource/lobby/Prefab/ChatLobby.prefab";
    public const string ItemTopHu = "Assets/Slot/Resource/popup/Prefab/ItemTopHu.prefab";

    #endregion


    #region Popup

    // ------------ Prefab ------------ //
    //DialogChangeAvatar
    public const string DialogChangeAvatar = "Assets/Slot/Resource/popup/Prefab/DialogChangeAvatar.prefab";

    public const string ItemAvatar = "Assets/Slot/Resource/popup/Prefab/ItemAvatar.prefab";

    //DialogChuyenVang
    public const string DialogChuyenVang = "Assets/Slot/Resource/popup/Prefab/DialogChuyenVang.prefab";
    public const string ItemLsChuyen = "Assets/Slot/Resource/popup/Prefab/ItemLsChuyen.prefab";

    public const string ItemLsNhan = "Assets/Slot/Resource/popup/Prefab/ItemLsNhan.prefab";

    //DialogDaiLy
    public const string DialogDaiLy = "Assets/Slot/Resource/popup/Prefab/DialogDaiLy.prefab";
    public const string ItemDaiLy = "Assets/Slot/Resource/popup/Prefab/ItemDaiLy.prefab";

    public const string ItemDaiLyDoiThuong = "Assets/Slot/Resource/popup/Prefab/ItemDaiLyDoiThuong.prefab";

    //DialogDoiThuong
    public const string DialogDoiThuong = "Assets/Slot/Resource/popup/Prefab/DialogDoiThuong.prefab";
    public const string ItemDoiThuong = "Assets/Slot/Resource/popup/Prefab/ItemDoiThuong.prefab";
    public const string ItemDoiThuongMomo = "Assets/Slot/Resource/popup/Prefab/ItemDoiThuongMomo.prefab";
    public const string ItemLichSuDoiThuong = "Assets/Slot/Resource/popup/Prefab/ItemLichSuDoiThuong.prefab";

    public const string VatPham = "Assets/Slot/Resource/popup/Sprite/VatPham/";

    //DialogInvitePlayer
    public const string DialogInvitePlayer = "Assets/Slot/Resource/popup/Prefab/DialogInvitePlayer.prefab";

    public const string ItemInvitePlayer = "Assets/Slot/Resource/popup/Prefab/ItemInvitePlayer.prefab";

    //DialogNapXu
    public const string DialogCuaHang = "Assets/Slot/Resource/popup/Prefab/DialogCuaHang.prefab";
    public const string ItemPriceCard = "Assets/Slot/Resource/popup/Prefab/ItemPriceCard.prefab";

    public const string ItemIAP = "Assets/Slot/Resource/popup/Prefab/ItemIAP.prefab";

    //DialogThongTinCaNhan
    public const string DialogThongTinCaNhan = "Assets/Slot/Resource/popup/Prefab/DialogThongTinCaNhan.prefab";
    public const string ItemLogGame = "Assets/Slot/Resource/popup/Prefab/ItemLogGame.prefab";
    public const string ItemGiaoDich = "Assets/Slot/Resource/popup/Prefab/ItemGiaoDich.prefab";
    public const string ItemEmail = "Assets/Slot/Resource/popup/Prefab/ItemEmail.prefab";

    public const string ItemHisTransaction = "Assets/Slot/Resource/popup/Prefab/ItemHisTransaction.prefab";

    // DialogBangXepHang
    public const string DialogBangXepHang = "Assets/Slot/Resource/popup/Prefab/DialogBangXepHang.prefab";
    public const string ItemTgBXH = "Assets/Slot/Resource/popup/Prefab/ItemTgBXH.prefab";

    public const string ItemTopPlayer = "Assets/Slot/Resource/popup/Prefab/ItemTopPlayer.prefab";

    //DialogEvent
    public const string DialogEvent = "Assets/Slot/Resource/popup/Prefab/DialogEvent.prefab";
    public const string ItemTgEvent = "Assets/Slot/Resource/popup/Prefab/ItemTgEvent.prefab";

    public const string DialogHuongDanGame = "Assets/Slot/Resource/popup/Prefab/DialogHuongDanGame.prefab";
    public const string DialogChangeDisplayName = "Assets/Slot/Resource/popup/Prefab/DialogChangeDisplayName.prefab";
    public const string DialogChangeInfo = "Assets/Slot/Resource/popup/Prefab/DialogChangeInfo.prefab";
    public const string DialogThongBaoExit = "Assets/Slot/Resource/popup/Prefab/DialogThongBaoExit.prefab";
    public const string DialogThongBao = "Assets/Slot/Resource/popup/Prefab/DialogThongBao.prefab";
    public const string ToastBg = "Assets/Slot/Resource/popup/Prefab/ToastBG.prefab";
    public const string DialogSetting = "Assets/Slot/Resource/popup/Prefab/DialogSetting.prefab";
    public const string DialogGiftCode = "Assets/Slot/Resource/popup/Prefab/DialogGiftCode.prefab";
    public const string DialogChatAdmin = "Assets/Slot/Resource/popup/Prefab/DialogChatAdmin.prefab";
    public const string DialogQuyDinh = "Assets/Slot/Resource/popup/Prefab/DialogQuyDinh.prefab";
    public const string DialogConfirm = "Assets/Slot/Resource/popup/Prefab/DialogConfirm.prefab";

    public const string DialogReceiverInvitePlayer = "Assets/Slot/Resource/popup/Prefab/DialogReceiverInvitePlayer.prefab";
    public const string DialogHuongDanMB = "Assets/Slot/Resource/popup/Prefab/DialogHuongDanMB.prefab";

    // ------------ Sprite ------------ //
    //DialogThongTinCaNhan
    public const string icon_mail = "Assets/Slot/Resource/popup/Sprite/icon_mail.png";
    public const string icon_mail_open = "Assets/Slot/Resource/popup/Sprite/icon_mail_open.png";
    public const string imgChuaXem = "Assets/Slot/Resource/popup/Sprite/imgChuaXem.png";

    public const string imgDaXem = "Assets/Slot/Resource/popup/Sprite/imgDaXem.png";

    // DialogBangXepHang
    public const string img_ngang_table_chan = "Assets/Slot/Resource/popup/Sprite/img_ngang_table_chan.png";
    public const string img_ngang_table_le = "Assets/Slot/Resource/popup/Sprite/img_ngang_table_le.png";

    #endregion


    #region SlotBongDa

    // ------------ Prefab ------------ //
    public const string SlotBongDa = "Assets/Slot/Resource/slotbongda/SlotBongDa.prefab";
    public const string GameBonusSub = "Assets/Slot/Resource/slotbongda/Prefab/GameBonusSub.prefab";
    public const string ItemGiftBonus = "Assets/Slot/Resource/slotbongda/Prefab/ItemGiftBonus.prefab";
    public const string ItemLSSlot = "Assets/Slot/Resource/slotbongda/Prefab/ItemLSSlot.prefab";
    public const string ItemTopVinhDanhTCT = "Assets/Slot/Resource/slotbongda/Prefab/ItemTopVinhDanhTCT.prefab";
    public const string ItemNoHuTCT = "Assets/Slot/Resource/slotbongda/Prefab/ItemNoHuTCT.prefab";
    public const string ItemGameBonusSub = "Assets/Slot/Resource/slotbongda/Prefab/ItemGameBonusSub.prefab";
    public const string ItemGameNhanDoi = "Assets/Slot/Resource/slotbongda/Prefab/ItemGameNhanDoi.prefab";
    public const string DialogBangThuong = "Assets/Slot/Resource/slotbongda/Prefab/GameManager/DialogBangThuong.prefab";
    public const string DialogHisTCT = "Assets/Slot/Resource/slotbongda/Prefab/GameManager/DialogHisTCT.prefab";
    public const string DialogThongBao2 = "Assets/Slot/Resource/slotbongda/Prefab/GameManager/DialogThongBao.prefab";
    public const string DialogTopTCT = "Assets/Slot/Resource/slotbongda/Prefab/GameManager/DialogTopTCT.prefab";
    public const string DialogGameBonus = "Assets/Slot/Resource/slotbongda/Prefab/GameManager/DialogGameBonus.prefab";
    public const string GameX2 = "Assets/Slot/Resource/slotbongda/Prefab/GameManager/GameX2.prefab";
    public const string MenuContainer = "Assets/Slot/Resource/slotbongda/Prefab/GameManager/MenuContainer.prefab";
    public const string NormalGameContainer = "Assets/Slot/Resource/slotbongda/Prefab/GameManager/NormalGameContainer.prefab";
    public const string AccumulateContainer = "Assets/Slot/Resource/slotbongda/Prefab/GameManager/AccumulateContainer.prefab";
    public const string FreeSpinContainer = "Assets/Slot/Resource/slotbongda/Prefab/GameManager/FreeSpinContainer.prefab";
    public const string EffectManager = "Assets/Slot/Resource/slotbongda/Prefab/GameManager/EffectManager.prefab";
    public const string SpinBonusBD = "Assets/Slot/Resource/slotbongda/Prefab/SpinBonus.prefab";
    public const string SpinSpinFreeBD = "Assets/Slot/Resource/slotbongda/Prefab/SpinFreeSpin.prefab";
    
    public const string EffectIconBd = "Assets/Slot/Resource/slotbongda/Prefab/Effect/item";

    // ------------ Sprite ------------ //
    public const string btn_dialog2_dis = "Assets/Slot/Resource/slotbongda/Sprite/btn_dialog2_dis.png";
    public const string btn_dialog2 = "Assets/Slot/Resource/slotbongda/Sprite/btn_dialog2.png";
    public const string x2 = "Assets/Slot/Resource/slotbongda/Sprite/x2.png";
    public const string x21 = "Assets/Slot/Resource/slotbongda/Sprite/x21.png";

    #endregion
    
    #region SlotFairyFish

    // ------------ Prefab ------------ //
    public const string SelectRoom = "Assets/Slot/Resource/slotfairyfish/Prefab/SelectRoom.prefab";
    public const string SlotFairyFish = "Assets/Slot/Resource/slotfairyfish/SlotFairyFish.prefab";
    public const string SlotFairyFishGame = "Assets/Slot/Resource/slotfairyfish/Prefab/GameManager/SlotFairyFishGame.prefab";
    public const string GameBonus = "Assets/Slot/Resource/slotfairyfish/Prefab/GameManager/GameBonus.prefab";
    public const string ItemGameBonus = "Assets/Slot/Resource/slotfairyfish/Prefab/ItemGameBonus.prefab";
    public const string ItemGameBonusLucky = "Assets/Slot/Resource/slotfairyfish/Prefab/ItemGameBonusLucky.prefab";
    public const string ItemLSFairyFish = "Assets/Slot/Resource/slotfairyfish/Prefab/ItemLSFairyFish.prefab";
    public const string IconCaHeo = "Assets/Slot/Resource/slotfairyfish/Prefab/IconCaHeo.prefab";
    public const string IconCaMap = "Assets/Slot/Resource/slotfairyfish/Prefab/IconCaMap.prefab";
    public const string IconCuaBien = "Assets/Slot/Resource/slotfairyfish/Prefab/IconCuaBien.prefab";
    public const string IconCaTien = "Assets/Slot/Resource/slotfairyfish/Prefab/IconCaTien.prefab";
    public const string ItemTopFairyFish = "Assets/Slot/Resource/slotfairyfish/Prefab/ItemTopFairyFish.prefab";
    public const string BangThuong = "Assets/Slot/Resource/slotfairyfish/Prefab/GameManager/BangThuong.prefab";
    public const string FairyFishHistory = "Assets/Slot/Resource/slotfairyfish/Prefab/GameManager/FairyFishHistory.prefab";
    public const string DialogThongBao3 = "Assets/Slot/Resource/slotfairyfish/Prefab/GameManager/DialogThongBao.prefab";
    public const string FairyFishTop = "Assets/Slot/Resource/slotfairyfish/Prefab/GameManager/FairyFishTop.prefab";
    public const string NormalGameContainerFF = "Assets/Slot/Resource/slotfairyfish/Prefab/GameManager/NormalGameContainer.prefab";
    public const string MenuContainerFF = "Assets/Slot/Resource/slotfairyfish/Prefab/GameManager/MenuContainer.prefab";
    public const string FreeSpinContainerFF = "Assets/Slot/Resource/slotfairyfish/Prefab/GameManager/FreeSpinContainer.prefab";
    public const string SelectLineTable = "Assets/Slot/Resource/slotfairyfish/Prefab/GameManager/SelectLineTable.prefab";
    public const string EffectManagerFF = "Assets/Slot/Resource/slotfairyfish/Prefab/GameManager/EffectManager.prefab";
    
    public const string NhanVatRoom1 = "Assets/Slot/Resource/slotfairyfish/Prefab/nhanvatRoom1.prefab";
    public const string NhanVatRoom2 = "Assets/Slot/Resource/slotfairyfish/Prefab/nhanvatRoom2.prefab";
    public const string NhanVatRoom3 = "Assets/Slot/Resource/slotfairyfish/Prefab/nhanvatRoom3.prefab";
    public const string NhanVatRoom4 = "Assets/Slot/Resource/slotfairyfish/Prefab/nhanvatRoom4.prefab";

    public const string SpinBonusRoom1FF = "Assets/Slot/Resource/slotfairyfish/Prefab/SpinBonusRoom1.prefab";
    public const string SpinBonusRoom2FF = "Assets/Slot/Resource/slotfairyfish/Prefab/SpinBonusRoom2.prefab";
    public const string SpinBonusRoom3FF = "Assets/Slot/Resource/slotfairyfish/Prefab/SpinBonusRoom3.prefab";
    public const string SpinBonusRoom4FF = "Assets/Slot/Resource/slotfairyfish/Prefab/SpinBonusRoom4.prefab";

    public const string SpinSpinFreeRoom1FF = "Assets/Slot/Resource/slotfairyfish/Prefab/SpinFreeSpinRoom1.prefab";
    public const string SpinSpinFreeRoom2FF = "Assets/Slot/Resource/slotfairyfish/Prefab/SpinFreeSpinRoom2.prefab";
    public const string SpinSpinFreeRoom3FF = "Assets/Slot/Resource/slotfairyfish/Prefab/SpinFreeSpinRoom3.prefab";
    public const string SpinSpinFreeRoom4FF = "Assets/Slot/Resource/slotfairyfish/Prefab/SpinFreeSpinRoom4.prefab";
    
    // Effect icon 
    // Room1
    public const string EffectIconAFf = "Assets/Slot/Resource/slotfairyfish/Prefab/Effect/Room1/itema";
    
    // Room2
    public const string EffectIconBFf = "Assets/Slot/Resource/slotfairyfish/Prefab/Effect/Room2/itemb";
    
    // Room3
    public const string EffectIconCFf = "Assets/Slot/Resource/slotfairyfish/Prefab/Effect/Room3/itemc";
    
    // Room4
    public const string EffectIconDFf = "Assets/Slot/Resource/slotfairyfish/Prefab/Effect/Room4/itemd";
    
    
    // ---- Sprites ---- //
    public const string iconDaoVang0FF = "Assets/Slot/Resource/slotfairyfish/Sprite/icondaovang0.png";
    public const string iconDaoVang1FF = "Assets/Slot/Resource/slotfairyfish/Sprite/icondaovang1.png";
    public const string iconDaoVang2FF = "Assets/Slot/Resource/slotfairyfish/Sprite/icondaovang2.png";
    public const string iconDaoVang3FF = "Assets/Slot/Resource/slotfairyfish/Sprite/icondaovang3.png";
    
    public const string iconBonus0FF = "Assets/Slot/Resource/slotfairyfish/Sprite/iconBonus0.png";
    public const string iconBonus1FF = "Assets/Slot/Resource/slotfairyfish/Sprite/iconBonus1.png";
    public const string iconBonus2FF = "Assets/Slot/Resource/slotfairyfish/Sprite/iconBonus2.png";

    #endregion


    #region slotsieuxe

    // ------------ Prefab ------------ //
    public const string SlotSieuXe = "Assets/Slot/Resource/slotsieuxe/SlotSieuXe.prefab";
    public const string GameBonusSubSX = "Assets/Slot/Resource/slotsieuxe/Prefab/GameBonusSub.prefab";
    public const string ItemGiftBonusSX = "Assets/Slot/Resource/slotsieuxe/Prefab/ItemGiftBonus.prefab";
    public const string ItemLSSlotSX = "Assets/Slot/Resource/slotbongda/Prefab/ItemLSSlot.prefab";
    public const string ItemGameBonusSubSX = "Assets/Slot/Resource/slotsieuxe/Prefab/ItemGameBonusSub.prefab";
    public const string ItemGameNhanDoiSieuXe = "Assets/Slot/Resource/slotsieuxe/Prefab/ItemGameNhanDoiSieuXe.prefab";
    public const string DialogBangThuongSieuXe = "Assets/Slot/Resource/slotsieuxe/Prefab/GameManager/DialogBangThuongSieuXe.prefab";
    public const string DialogHistorySieuXe = "Assets/Slot/Resource/slotsieuxe/Prefab/GameManager/DialogHistorySieuXe.prefab";
    public const string DialogThongBaoSX = "Assets/Slot/Resource/slotsieuxe/Prefab/GameManager/DialogThongBao.prefab";
    public const string DialogTopSieuXe = "Assets/Slot/Resource/slotsieuxe/Prefab/GameManager/DialogTopSieuXe.prefab";
    public const string DialogGameBonusSX = "Assets/Slot/Resource/slotsieuxe/Prefab/GameManager/DialogGameBonus.prefab";
    public const string GameX2SX = "Assets/Slot/Resource/slotsieuxe/Prefab/GameManager/GameX2.prefab";
    public const string MenuContainerSX = "Assets/Slot/Resource/slotsieuxe/Prefab/GameManager/MenuContainer.prefab";
    public const string NormalGameContainerSX = "Assets/Slot/Resource/slotsieuxe/Prefab/GameManager/NormalGameContainer.prefab";
    public const string AccumulateContainerSX = "Assets/Slot/Resource/slotsieuxe/Prefab/GameManager/AccumulateContainer.prefab";
    public const string FreeSpinContainerSX = "Assets/Slot/Resource/slotsieuxe/Prefab/GameManager/FreeSpinContainer.prefab";
    public const string EffectManagerSX = "Assets/Slot/Resource/slotsieuxe/Prefab/GameManager/EffectManager.prefab";

    #endregion

    #region Slot4View

    // ------------ Prefab ------------ //
    public const string Slot4View = "";

    #endregion

    /// <summary>
    /// LOAD ALL PREFAB
    /// </summary>
    // ------------ TexureAtlas ---------- //
    public const string sieuxeUI = "Assets/Slot/Resource/slotsieuxe/TextureAtlas/sieuxeUI.png";

    public const string BauCuaUI = "Assets/Slot/Resource/gamemini/TextureAtlas/BauCuaUI.png";
    
    public const string GameMiniUI = "Assets/Slot/Resource/gamemini/TextureAtlas/GameMiniUI.png";
    public const string cardall = "Assets/Slot/Resource/gamemini/TextureAtlas/cardall.png";
    public const string gameTypeId = "Assets/Slot/Resource/game/Sprite/Gameid";
    public const string SpriteGame = "Assets/Slot/Resource/game/Sprite/";
    public const string xucxac = "Assets/Slot/Resource/gamemini/Sprite/xucxac.png";
    public const string lobbyUI = "Assets/Slot/Resource/lobby/TextureAtlas/lobbyUI.png";
    public const string GameBaiUI = "Assets/Slot/Resource/game/TextureAtlas/GameBaiUI.png";
    public const string commonUI = "Assets/Slot/Resource/common/TextureAtlas/commonUI.png";
    public const string MainUIBD = "Assets/Slot/Resource/slotbongda/TexureAtlas/MainUI.png";
    public const string MainUICaTien = "Assets/Slot/Resource/slotfairyfish/TextureAtlas/MainUI.png";
    public const string CaTienUI = "Assets/Slot/Resource/slotfairyfish/TextureAtlas/CaTienUI.png";
    public const string EmotionUI = "Assets/Slot/Resource/common/TextureAtlas/EmotionUI.png";
    public const string CardallUI = "Assets/Slot/Resource/game/TextureAtlas/CardallUI.png";

    /// <summary>
    /// Chi tiết TexureAtlas
    /// </summary>
    // LobbyUI
    public const string Lobby_Room1 = "imgNenChon";
    public const string Lobby_Room2 = "imgNenChon2";
    // public const string Lobby_CoTruyenHu = "imgIconCoTruyenHu";
    // public const string Lobby_IconChienBinh = "imgIconNuHoangChienBinh";
    public const string Lobby_IconSanRong = "Assets/Slot/Resource/lobby/Sprite/imgIconSanRong.png";
    public const string Lobby_IconNuHoang = "Assets/Slot/Resource/lobby/Sprite/imgIconNuHoang.png";
    public const string Lobby_IconPokerHu = "imgIconPokerHu";
    

}