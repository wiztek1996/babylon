﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Helper : MonoBehaviour
{
    /// <summary>
    /// Call from StartCoroutine()
    /// </summary>
    internal static IEnumerator LoadSceneAsync(string nameScene)
    {
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(nameScene);
        asyncOperation.allowSceneActivation = false;
        while (!asyncOperation.isDone)
        {
            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f)
            {
                //Activate the Scene
                asyncOperation.allowSceneActivation = true;
            }
            yield return null;
        }
        yield return null;
    }
    /// <summary>
    /// Call from StartCoroutine()
    /// </summary>
    internal static IEnumerator LoadSceneAsync(string nameScene, Text txtPercent)
    {
        //Begin to load the Scene you specify
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(nameScene);
        //Don't let the Scene activate until you allow it to
        asyncOperation.allowSceneActivation = false;
        //When the load is still in progress, output the Text and progress bar
        while (!asyncOperation.isDone)
        {
            //Output the current progress
            txtPercent.text = (asyncOperation.progress * 100) + "%";

            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f)
            {
                //Activate the Scene
                asyncOperation.allowSceneActivation = true;
            }
            yield return null;
        }
        yield return null;
    }
    public static void MoveLogin()
    {
        //LoginController.Instance.MoveMapGame(true);
    }


    //public static string EncodeSHA1(string pass)
    //{
    //    SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider();
    //    byte[] bs = System.Text.Encoding.UTF8.GetBytes(pass);
    //    bs = sha1.ComputeHash(bs);
    //    System.Text.StringBuilder s = new System.Text.StringBuilder();
    //    foreach (byte b in bs)
    //    {
    //        s.Append(b.ToString("x1").ToLower());
    //    }
    //    pass = s.ToString();
    //    return pass;
    //}

    //public static string Base64Decode(string base64EncodedData)
    //{
    //    byte[] base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
    //    return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
    //}
    private static string CHUOI_CHECK_VALIDATE = "1234567890_QWERTYUIOASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";//Các kí tự đăng nhập
    /// <summary>
    /// Trả về FALSE nếu không hợp lệ
    /// </summary>
    public static bool CheckUsernameValidate(string username)
    {
        foreach (char kiTu in username)
        {
            bool check = false;
            foreach (char kitu2 in CHUOI_CHECK_VALIDATE)
            {
                if (kiTu == kitu2)
                {
                    check = true;
                    break;
                }
            }
            if (!check)
            { return false; }
        }
        return true;
    }
    private static string CHUOI_CHECK_PASSWORD = "1234567890";
    /// <summary>
    /// Check pasword có cả chữ và số
    /// </summary>
    public static bool CheckPasswordValidate(string password)
    {
        bool checkNumber = false;
        bool checkText = false;
        foreach (char kiTu in password)
        {
            foreach (char kitu2 in CHUOI_CHECK_PASSWORD)
            {
                if (!checkNumber)
                {
                    if (kiTu == kitu2)
                    {
                        checkNumber = true;
                        break;
                    }
                }
            }
        }
        foreach (char kiTu in password)
        {
            if (!char.IsDigit(kiTu))
            {
                checkText = true; break;
            }
        }
        if (checkNumber && checkText)
        {
            return true;
        }
        return false;
    }


    public static string ShowMoney(int money)
    {
        return ConverMoney(long.Parse(money.ToString()));
    }
    
    public static string ShowMoney2(int money)
    {
        return ConverMoney2(long.Parse(money.ToString()));
    }
    
    public static string ShowMoney2(long money)
    {
        return ConverMoney2(long.Parse(money.ToString()));
    }

    public static string ShowMoney(long money)
    {
        return ConverMoney(money);
    }
    public static string ShowMoneyAll(long money)
    {
        return ConverMoneyHu(money);
    }  
    
    public static string ShowMoneyAll2(long money)
    {
        return ConverMoney2(money);
    }  
    
    static string ConverMoney2(long money)
    {
        string ss = "";
        if (money >= -100L && money <= 100L) return money.ToString();
        else if (money > 100 && money < 1000)
        {
            ss = money.ToString("0,0");
        }
        else if (money >= 1000 && money < 100000)
        {
            ss = (money / 1000) + "K";
        }
        else if (money >= 100000 && money <= 100000000)
        {
            ss = (money / 1000).ToString("0,0") + "K";
        }
        else if (money >= 100000000)
        {
            ss = (money / 1000000).ToString("0,0") + "M";
        }
        return ss != "00" ? ss : "0";
    }

    static string ConverMoney(long money)
    {
        string ss = "";
        if (money >= -100L && money <= 100L) return money.ToString();
        else if (money > 100 && money <= 1000000)
        {
            ss = money.ToString("0,0");
        }
        else if (money >= 1000000 && money <= 100000000)
        {
            ss = (money / 1000).ToString("0,0") + "K";
        }
        else if (money >= 100000000)
        {
            ss = (money / 1000000).ToString("0,0") + "M";
        }
        return ss != "00" ? ss : "0";
    }
    
    static string ConverMoneyHu(long money)
    {
        string ss = "";
        if (money >= -100L && money <= 100L) return money.ToString();
        else if (money > 100 && money <= 1000000)
        {
            ss = money.ToString("0,0");
        }
        else if (money >= 1000000 && money <= 100000000)
        {
            ss = money.ToString("0,0,0")+ "";
        }
        else if (money >= 100000000)
        {
            ss = (money / 1000000).ToString("0,0") + "M";
        }
        return ss != "00" ? ss : "0";
    }
    /// <summary>
    /// Hiện tiền có dấu chấm phân cách
    /// </summary>
    /// <returns></returns>
    public static string ShowMoneyDotDetail(long money)
    {
        return ConverMoneyDotDetail(money);
    }
    private static string ConverMoneyDotDetail(long money)
    {
        string ss = "";
        if (money < 1000L) return money.ToString();
        else if (money >= 1000 && money < 1000000)
        {
            long temp = money / 1000;
            //ss = temp >= 10L ? temp.ToString("0,0") + "K" : temp + "K";
            if (temp >= 0)
            {
                double a = (double)money / 1000L;
                ss = string.Format("{0:0.0}", a);
                string[] arr = ss.Split('.');
                bool ck = false;
                if (int.Parse(arr[1]) == 0)
                {
                    ss = arr[0] + "K";
                    ck = true;
                }
                if (!ck)
                {
                    ss += "K";
                }
            }
            else { ss = temp + "K"; }
        }
        else if (money >= 1000000)
        {
            long temp = money / 1000000;
            if (temp >= 0)
            {
                double a = (double)money / 1000000L;
                ss = string.Format("{0:0.0}", a) + "M";
            }
            else { ss = temp + "M"; }
            //ss = temp >= 10L ? temp.ToString("0,0") + "M" : temp + "M";
        }
        return ss != "00" ? ss : "0";
    }

    public static int RandomNumberGenerator(int min, int max)
    {
        return UnityEngine.Random.Range(min, max);
    }

    /// <summary>
    /// Get color by index màu RGB
    /// </summary>
    public static Color GetColor(float colorIndex)
    {
        Color color = Color.green;
        float max = 255F;
        float r = colorIndex / max;
        float g = colorIndex / max;
        float b = colorIndex / max;
        float a = 1F;
        color = new Color(r, g, b, a);
        return color;
    }
    public static Color GetColor(float _r, float _g, float _b)
    {
        Color color = Color.green;
        float max = 255F;
        float r = _r / max;
        float g = _g / max;
        float b = _b / max;
        float a = 1F;
        color = new Color(r, g, b, a);
        return color;
    }
    /// <summary>
    /// Lấy màu làm mờ ảnh
    /// </summary>
    /// <returns></returns>
    public static Color GetColor()
    {
        Color color = Color.green;
        float m = 255F;
        color = new Color(m, m, m, 0F);
        return color;
    }

    public static void DestroyAllChilds(Transform parent)
    {
        foreach (Transform t in parent)
        {
            GameObject.Destroy(t.gameObject);
        }
    }

    public static string GetNameGame(int gameId)
    {
        string gameName = "Game";
        switch (gameId)
        {
            case GameID.TLMN:
                gameName = "Tiến lên";
                break;  
            case GameID.TLMNSL:
                gameName = "Tiến lên Solo";
                break;   
            case GameID.BA_CAY:
                gameName = "Ba cây";
                break;
            case GameID.PHOM:
                gameName = "Phỏm";
                break;           
            case GameID.MAU_BINH:
                gameName = "Mậu binh";
                break;
            case GameID.SAM:
                gameName = "Sâm";
                break;
            case GameID.XOC_DIA:
                gameName = "Xóc Đĩa";
                break;
        }
        return gameName;
    }
    public static string GetNameGameInTable(int gameId)
    {

        string gameName = "Game";
        switch (gameId)
        {
            case GameID.TLMN:
                gameName = "TIẾN LÊN MIỀN NAM";
                break; 
            case GameID.TLMNSL:
                gameName = "TIẾN LÊN MIỀN NAM SOLO ";
                break;   
            case GameID.BA_CAY:
                gameName = "Ba Cây";
                break;
            case GameID.PHOM:
                gameName = "Phỏm";
                break;            
            case GameID.MAU_BINH:
                gameName = "Mậu Binh";
                break;
            case GameID.SAM:
                gameName = "Sâm";
                break;
            case GameID.XOC_DIA:
                gameName = "XÓC ĐĨA";
                break;           
            default:
                break;
        }
        return gameName;
    }
    private static float TIME_EFFECT = 0.6F;
    /// <summary>
    /// Hiệu ứng nhấn nút rate
    /// </summary>
    public static void ClickEffect(Transform obj,float rate)
    {
        float scale = rate * 1.15f;
        obj.DOScale(new Vector3(scale, scale, scale), TIME_CLICK).OnComplete(delegate { ReverseScale(obj,rate); });
    }
    
    /// <summary>
    /// Hiệu ứng nhấn nút rate
    /// </summary>
    public static void ClickEffectOffline(Transform obj,float rate)
    {
        float scale = rate * 1.3f;
        obj.DOScale(new Vector3(scale, scale, scale), TIME_CLICK).OnComplete(delegate { ReverseScale(obj,rate); });
    }
    private static float TIME_CLICK = 0.2F;
    /// <summary>
    /// Hiệu ứng nhấn nút
    /// </summary>
    public static void ClickEffect(Transform obj)
    {
        obj.localScale = Vector3.one;
        float rate = 1.1F;
        obj.DOScale(new Vector3(rate, rate, rate), TIME_CLICK).OnComplete(delegate { ReverseScale(obj); });
    }
    
    public static void ClickEffectReal(Transform obj, float scaleF)
    {
        obj.localScale = Vector3.one;
        float scale = scaleF;
        obj.DOScale(new Vector3(scale, scale, scale), TIME_CLICK).OnComplete(delegate { ReverseScale(obj); });
    }

    public static void ClickEffectNomal(Transform obj)
    {
        obj.localScale = Vector3.one;
        float rate = 1F;
        obj.DOScale(new Vector3(rate, rate, rate), TIME_CLICK).OnComplete(delegate { ReverseScale(obj); });
    }

    private static void ReverseScale(Transform obj ,float scale)
    {
        obj.DOScale(new Vector3(scale,scale,1), TIME_CLICK);
    }
    
    private static void ReverseScale(Transform obj)
    {
        obj.DOScale(Vector3.one, TIME_CLICK);
    }
    private const float TIME_MOVE_TRANFORM = 0.4F;
    public static void DoLocalMove(Transform tran, Vector3 target)
    {
        tran.DOLocalMove(target, TIME_MOVE_TRANFORM);
    }
    /// <summary>
    /// Gọi trong hàm StartCoroutine()
    /// </summary>
    public static IEnumerator LoadPictureFromInternet(Image img, string linkPicture)
    {
        WWW www = new WWW(linkPicture);
        yield return www;
        if (www != null)
        {
            Texture2D tex = ChangeFormat(www.texture, C.TEXTURE_FORMAT_DOWNLOADED_IMG);
            img.sprite = Sprite.Create(tex, new Rect(0, 0, www.texture.width, www.texture.height), Vector2.zero);
            img.gameObject.SetActive(true);
        }
    }
    
    public static Texture2D ChangeFormat(Texture2D oldTexture, TextureFormat newFormat)
    {
        //Create new empty Texture
        Texture2D newTex = new Texture2D(oldTexture.width, oldTexture.height, newFormat, false);
        //Copy old texture pixels into new one
        newTex.SetPixels(oldTexture.GetPixels());
        //Apply
        newTex.Apply();

        return newTex;
    }
    
    /// <summary>
    /// Gọi trong hàm StartCoroutine()
    /// </summary>
    public static IEnumerator DisableObject(GameObject obj, float time)
    {
        yield return new WaitForSeconds(time);
        obj.SetActive(false);
    }
    public static void SetVisible(GameObject obj, bool st)
    {
        obj.SetActive(st);
    }
    /// <summary>
    /// Get time miliseconds
    /// </summary>
    /// <returns></returns>
    public static long GetCurrentMilli()
    {
        TimeSpan ts = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
        long millis = (long)ts.TotalMilliseconds;
        return millis;
    }
    public static string ShowTimeFromMiliseconds(long miliSeconds)
    {
        double ticks = miliSeconds;
        TimeSpan ts = TimeSpan.FromMilliseconds(ticks);
        // Múi h Việt Nam là +7
        DateTime time = new DateTime(1970, 1, 1, 7, 0, 0, DateTimeKind.Utc) + ts; ;
        string format = "HH:mm dd-MM-yyyy";   
        return time.ToString(format);
    }

   
    public static bool CheckGameIsReady(sbyte gameId)
    {
        if (gameId == GameID.TLMN || gameId == GameID.TLMNSL|| gameId == GameID.PHOM || gameId == GameID.SAM)
        {
            return true;
        }
        return false;
    }
    /// <summary>
    /// Rung theo theo số seconds
    /// </summary>
    /// <param name="time"></param>
    internal static void Vibrate()
    {
        if (PlayerPrefs.GetInt(C.PP_VIBRATE) == 1)
        {
#if NETFX_CORE || WINDOWS_PHONE ||UNITY_IOS || UNITY_ANDROID || UNITY_EDITOR
            Handheld.Vibrate();
#endif
        }
    }

    public static string NumberGroup(long strNumber)
    {
        return (strNumber <= 0) ? "0" : string.Format("{0:##,##}", strNumber).Replace(",", ".");
    }

    public static List<object> CovertToLstObj<T>(List<T> lst)
    {
        List<object> lstObj = new List<object>();
        foreach (var item in lst)
        {
            lstObj.Add(item);
        }

        return lstObj;
    }
    private static float TIME_PREVIEW_EFFECT = 1.3F;
    public static void ClickLoopEffect(Transform obj,int loopCount)
    {
        obj.DOScale(new Vector3(1.18f, 1.18f, 1.18f), TIME_PREVIEW_EFFECT).SetLoops(loopCount).OnComplete(delegate { ReverseScale(obj); });
    }
}
