﻿using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AgentUnity
{
    public static string GetIMEI()
    {
        string imei = "";
        imei = SystemInfo.deviceUniqueIdentifier;
        return imei;
    }
    /// <summary>
    /// Chuyển scene
    /// </summary>
    public static void MoveScene(string nameScene)
    {
        //SceneManager.LoadScene(nameScene);
        AsyncOperation async = SceneManager.LoadSceneAsync(nameScene);
        async.allowSceneActivation = true;
    }
    public static IEnumerator MoveSceneAsync(string nameScene)
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(nameScene);
        async.allowSceneActivation = true;
        while (!async.isDone)
        {
            yield return new WaitForEndOfFrame();
        }
    }
    public static int RandomNumberGenerator(int min, int max)
    {
        return Random.Range(min, max);
    }

    /// <summary>
    /// Get color by index màu RGB
    /// </summary>
    public static Color GetColor(float colorIndex)
    {
        Color color = Color.green;
        float max = 255F;
        float r = colorIndex / max;
        float g = colorIndex / max;
        float b = colorIndex / max;
        float a = 1F;
        color = new Color(r, g, b, a);
        return color;
    }
    public static Color GetColor(float _r, float _g, float _b)
    {
        Color color = Color.green;
        float max = 255F;
        float r = _r / max;
        float g = _g / max;
        float b = _b / max;
        float a = 1F;
        color = new Color(r, g, b, a);
        return color;
    }
    /// <summary>
    /// Lấy màu làm mờ ảnh
    /// </summary>
    /// <returns></returns>
    public static Color GetColor()
    {
        Color color = Color.green;
        float m = 255F;
        color = new Color(m, m, m, 0F);
        return color;
    }

    public static void DestroyAllChilds(Transform parent)
    {
        foreach (Transform t in parent)
        {
            GameObject.Destroy(t.gameObject);
        }
    }

    private static float TIME_CLICK = 0.25F;
    /// <summary>
    /// Hiệu ứng nhấn nút
    /// </summary>
    public static void ClickEffect(Transform obj)
    {
        obj.localScale = Vector3.one;
        float rate = 1.1F;
        obj.DOScale(new Vector3(rate, rate, rate), TIME_CLICK).OnComplete(delegate { ReverseScale(obj); });
    }
    private static void ReverseScale(Transform obj)
    {
        obj.DOScale(Vector3.one, TIME_CLICK);
    }
    private const float TIME_MOVE_TRANFORM = 0.4F;
    public static void DoLocalMove(Transform tran, Vector3 target)
    {
        tran.DOLocalMove(target, TIME_MOVE_TRANFORM);
    }
    public static void DoLocalMove(Transform tran, Vector3 target, float timeMove)
    {
        tran.DOLocalMove(target, timeMove);
    }
    public static void DoLocalAndScale(Transform tran, Vector3 target, Vector3 scale, float timeMove)
    {
        tran.DOLocalMove(target, timeMove);
        tran.DOScale(scale, timeMove);
    }
    /// <summary>
    /// Gọi trong hàm StartCoroutine()
    /// </summary>
    public static IEnumerator LoadPictureFromInternet(Image img, string linkPicture)
    {
        WWW www = new WWW(linkPicture);
        yield return www;
        if (www != null)
        {
            img.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), Vector2.zero);
            img.gameObject.SetActive(true);
        }
    }
    /// <summary>
    /// Gọi trong hàm StartCoroutine()
    /// </summary>
    public static IEnumerator DisableObject(GameObject obj, float time)
    {
        yield return new WaitForSeconds(time);
        obj.SetActive(false);
    }
    public static void SetVisible(GameObject obj, bool st)
    {
        obj.SetActive(st);
    }
    /// <summary>
    /// Rung theo theo số seconds
    /// </summary>
    /// <param name="time"></param>
    internal static void Vibrate()
    {
        if (PlayerPrefs.GetInt(C.PP_VIBRATE) == 1)
        {
#if NETFX_CORE || WINDOWS_PHONE ||UNITY_IOS || UNITY_ANDROID || UNITY_EDITOR
            Handheld.Vibrate();
#endif
        }
    }
    public static bool CheckNetWork()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            return false;
        }
        return true;
    }
    public static bool CheckSound()
    {
        if (PlayerPrefs.GetInt(C.PP_MUSIC) == C.TRUE)
        {
            return true;
        }
        return false;
    }
    internal static void LogWarning(object obj)
    {
#if UNITY_EDITOR
        Debug.LogWarning(obj);
#endif
    }
    internal static void LogError(object obj)
    {
#if UNITY_EDITOR
        Debug.LogError(obj);
#endif
    }
    internal static GameObject InstanceObject(GameObject prefab, Transform parent)
    {
        GameObject tmp = GameObject.Instantiate(prefab);
        tmp.transform.SetParent(parent);
        tmp.transform.localPosition = Vector3.zero;
        tmp.transform.localScale = Vector3.one;
        return tmp;
    }
    internal static GameObject InstanceObject(GameObject prefab, Transform parent, Vector3 position)
    {
        GameObject tmp = GameObject.Instantiate(prefab);
        tmp.transform.SetParent(parent);
        tmp.transform.localPosition = position;
        tmp.transform.localScale = Vector3.one;
        return tmp;
    }
    
    internal static T InstanceObject<T>(GameObject prefab, Transform parent, Vector3 position)
    {
        GameObject tmp = GameObject.Instantiate(prefab);
        tmp.transform.SetParent(parent);
        tmp.transform.localPosition = position;
        tmp.transform.localScale = Vector3.one;
        return tmp.GetComponent<T>();
    }
    /* Lấy thông tin đối tượng chạm trên màn hình */
    internal static GameObject TouchInfo(Camera camera, Vector3 pos)
    {
        Vector3 wp = camera.ScreenToWorldPoint(pos);
        Vector2 touchPos = new Vector2(wp.x, wp.y);
        GameObject ObjPointer = null;
        if (Physics2D.OverlapPoint(touchPos))   //OverlapPoint: trả về đối tượng va chạm
            ObjPointer = Physics2D.OverlapPoint(touchPos).gameObject;

        return ObjPointer;
    }
    internal static void CreateMapIndexAndMapPosition(ref int[,] mapIndex, ref Vector2[,] mapPos, int width, int height, int defaultIndex,
        float offsetX, float offsetY)
    {
        mapIndex = new int[width, height];
        mapPos = new Vector2[width, height];
        //float checkX = offsetX * (float)width / 2F - offsetX / 2F;
        //float checkY = offsetY * (float)height / 2F * 0.5F - offsetY / 2F;
        float checkX = offsetX * (float)width / 2F * 0.5F - offsetX / 2F;
        float checkY = offsetY * (float)height / 2F * 0.5F - offsetY / 2F;
        System.Text.StringBuilder sb;
        for (int i = 0; i < width; i++)
        {
            /***Map so le check cheo***/
            sb = new System.Text.StringBuilder();
            float y = (float)i * offsetY * 0.5F - checkY;
            for (int j = 0; j < height; j++)
            {
                mapIndex[i, j] = defaultIndex;
                mapPos[i, j] = new Vector2((float)j * offsetX * 0.5F - checkX, y + ((float)j * offsetY * 0.5F));
                sb.Append(mapPos[i, j]).Append(" - ");
            }
            //Debug.Log(sb);

            ///***Map so le***/
            ////sb = new System.Text.StringBuilder();
            //float y = (float)i * offsetY *0.5F - checkY;
            //for (int j = 0; j < height; j++)
            //{
            //    mapIndex[i, j] = defaultIndex;
            //    if (i % 2 == 0)
            //    {
            //        mapPos[i, j] = new Vector2((j * offsetX - checkX) + offsetX / 2F, y);
            //    }
            //    else
            //    {
            //        mapPos[i, j] = new Vector2(j * offsetX - checkX, y);
            //    }
            //    //sb.Append(mapPos[i, j]).Append(" - ");
            //}
            ////Debug.Log(sb);

            /***Map vuong***/
            ////System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //float y = i * offsetY - checkY;
            //for (int j = 0; j < height; j++)
            //{
            //    mapIndex[i, j] = defaultIndex;
            //    mapPos[i, j] = new Vector2(j * offsetX - checkX, y);
            //    //sb.Append(mapPos[i, j]).Append(" - ");
            //}
            ////Debug.Log(sb);
        }
    }

    internal static T InstanceObject<T>(GameObject prefab, Transform parent)
    {
        GameObject tmp = GameObject.Instantiate(prefab);
        tmp.transform.SetParent(parent);
        tmp.transform.localPosition = Vector3.zero;
        tmp.transform.localScale = Vector3.one;
        return tmp.GetComponent<T>();
    }
    internal static T InstanceObject<T>(Behaviour prefab, Transform parent)
    {
        GameObject tmp = GameObject.Instantiate(prefab.gameObject);
        tmp.transform.SetParent(parent);
        tmp.transform.localPosition = Vector3.zero;
        tmp.transform.localScale = Vector3.one;
        return tmp.GetComponent<T>();
    }
    internal static T InstanceObjectUIStretch<T>(GameObject prefab, Transform parent)
    {
        GameObject tmp = GameObject.Instantiate(prefab);
        tmp.transform.SetParent(parent);
        tmp.transform.localPosition = Vector3.zero;
        tmp.transform.localScale = Vector3.one;
        tmp.transform.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
        return tmp.GetComponent<T>();
    }

    internal static T InstanceObjectUIStretch<T>(Behaviour prefab, Transform parent)
    {
        GameObject tmp = GameObject.Instantiate(prefab.gameObject);
        tmp.transform.SetParent(parent);
        tmp.transform.localPosition = Vector3.zero;
        tmp.transform.localScale = Vector3.one;
        tmp.transform.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
        return tmp.GetComponent<T>();
    }
    
    /// <summary>
    /// LOG tất cả giá trị của 1 class
    /// </summary>
    /// <param name="obj"></param>
    // public static void DebugFullClass(object obj)
    // {
    //     string log = ObjectDumper.Dump(obj);
    //     Debug.LogError("CLASS : "+obj.GetType()+".cs" + "\n" +log);
    // }

    internal static string ShowMoneyFull(long money)
    {
        return ConverMoneyFull(money);
    }
    
    static string ConverMoneyFull(long money)
    {
        string ss = "";
        
        if (money >= -100L && money <= 100L) return money.ToString();
        else if (money > 100 && money < 1000000)
        {
            ss = money.ToString("0,0");
        }
        else if (money >= 1000000 && money <= 999999999)
        {
            ss = money.ToString("0,0,0");
        }
        else if (money >= 1000000000)
        {
            ss = (money / 1000).ToString("0,0,0") + "K";
//            ss = money.ToString("0,0,0,0");
        }
        return ss != "00" ? ss : "0";
    }
    
}
