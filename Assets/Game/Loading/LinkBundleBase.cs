﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkBundleBase {
    public LinkBundleBase(int providerId, string linkBundle, int version)
    {
        ProviderId = providerId;
        LinkBundle = linkBundle;
        Version = version;
    }

    public int ProviderId { get; set; }
    public string LinkBundle { get; set; }
    public int Version { get; set; }
}
