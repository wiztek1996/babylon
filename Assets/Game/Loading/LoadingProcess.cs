﻿#if NETFX_CORE || WINDOWS_PHONE
#else
using LitJson;
#endif
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingProcess : MonoBehaviour
{
    public Text txtPercent;



    private void Awake()
    {
        Application.targetFrameRate = C.TARGET_FRAME;
    }

    private void Start()
    {
        Application.runInBackground = true;
        StartCoroutine(Helper.LoadSceneAsync(MoveScene.MAP_GAME, txtPercent));
    }





    //    public Text txtProcess;
    //    public Slider slProcess;

    //    public string[] linkBundles;

    //    List<LinkBundleBase> _lstLinkBundles = new List<LinkBundleBase>();
    //    public List<AssetBundle> lstAssetBundle = new List<AssetBundle>();
    //    //AssetBundle _bundlePrefab;
    //    private const int VERSION_BUNDLE = 1;

    //    private void Awake()
    //    {
    //        //btnQuit.onClick.AddListener(() => { Application.Quit(); });
    //        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    //    }

    //    private void Start()
    //    {
    //        _lstLinkBundles.Add(new LinkBundleBase(CMD.PROVIDER_ID, "file:///E:/Duong/Unity/VerKing/Assets/AssetBundles/StandaloneWindows/avata", 1));
    //        _lstLinkBundles.Add(new LinkBundleBase(CMD.PROVIDER_ID, "file:///E:/Duong/Unity/VerKing/Assets/AssetBundles/StandaloneWindows/fonts", 1));
    //        _lstLinkBundles.Add(new LinkBundleBase(CMD.PROVIDER_ID, "file:///E:/Duong/Unity/VerKing/Assets/AssetBundles/StandaloneWindows/prefabs", 1));
    //        _lstLinkBundles.Add(new LinkBundleBase(CMD.PROVIDER_ID, "file:///E:/Duong/Unity/VerKing/Assets/AssetBundles/StandaloneWindows/scenes", 1));
    //        _lstLinkBundles.Add(new LinkBundleBase(CMD.PROVIDER_ID, "file:///E:/Duong/Unity/VerKing/Assets/AssetBundles/StandaloneWindows/sounds", 1));
    //        _lstLinkBundles.Add(new LinkBundleBase(CMD.PROVIDER_ID, "file:///E:/Duong/Unity/VerKing/Assets/AssetBundles/StandaloneWindows/sprites", 1));
    //        _lstLinkBundles.Add(new LinkBundleBase(CMD.PROVIDER_ID, "file:///E:/Duong/Unity/VerKing/Assets/AssetBundles/StandaloneWindows/sprites2", 1));
    //        _lstLinkBundles.Add(new LinkBundleBase(CMD.PROVIDER_ID, "file:///E:/Duong/Unity/VerKing/Assets/AssetBundles/StandaloneWindows/sprites3", 1));
    //        _lstLinkBundles.Add(new LinkBundleBase(CMD.PROVIDER_ID, "file:///E:/Duong/Unity/VerKing/Assets/AssetBundles/StandaloneWindows/StandaloneWindows", 1));
    //        _lstLinkBundles.Add(new LinkBundleBase(CMD.PROVIDER_ID, "file:///E:/Duong/Unity/VerKing/Assets/AssetBundles/StandaloneWindows/ui", 1));

    //        //Debug.Log(_lstLinkBundles.Count);
    //        StartCoroutine(LoadBundle(_lstLinkBundles));
    //    }

    //    private string LINK_ALL_BUNDLE = "http://webservices.vuabida.com/webservice/dcm2.php?provider_id=";
    //    private const string THONG_BAO = "Có lỗi trong quá trình tải dữ liệu";
    //    private IEnumerator LoadLinkBundle()
    //    {
    //        string url = LINK_ALL_BUNDLE;
    //        url += CMD.PROVIDER_ID;
    //        url += "&version_game=" + Res.VERSION;
    //        url += "&format=json";
    //        WWW www = new WWW(url);
    //        //WWWForm form = new WWWForm();
    //        //form.AddField("userId", B.Instance.MainInfo.UserID + "");

    //        //WWW www = new WWW(url, form);
    //        yield return www;

    //        if (www.text != null)
    //        {
    //            if (www.text.Length > 100)
    //            {
    //#if NETFX_CORE || WINDOWS_PHONE
    //#else
    //                JsonData jsonData = JsonMapper.ToObject(www.text);
    //                //bool isSetPhoneCskh = false;
    //                if (jsonData.Count > 0)
    //                {
    //                    for (int i = 0; i < jsonData.Count; i++)
    //                    {
    //                        int providerId = int.Parse(jsonData[i]["provider_id"].ToString());
    //                        string linkBundle = jsonData[i]["link_bundle"].ToString();
    //                        int version = int.Parse(jsonData[i]["version"].ToString());

    //                        if (providerId == CMD.PROVIDER_ID)
    //                        {
    //                            _lstLinkBundles.Add(new LinkBundleBase(providerId, linkBundle, version));
    //                        }
    //                    }

    //                    if (_lstLinkBundles.Count > 2)
    //                    {
    //                        StartCoroutine(LoadBundle(_lstLinkBundles));
    //                    }
    //                }
    //#endif
    //            }
    //            else
    //            {
    //                txtProcess.text = THONG_BAO;
    //            }
    //        }
    //        else
    //        {
    //            txtProcess.text = THONG_BAO;
    //        }
    //    }

    //    IEnumerator LoadBundle(List<LinkBundleBase> linkBundles)
    //    {
    //        while (!Caching.ready)
    //            yield return null;

    //        /* Load all AssetsBundle temp */
    //        int _number = 0;
    //        int maxLink = linkBundles.Count;
    //        txtProcess.text = "0 / 100";
    //        foreach (LinkBundleBase link in linkBundles)
    //        {
    //            AssetBundle _assetBundle;
    //            WWW bundle = WWW.LoadFromCacheOrDownload(link.LinkBundle, VERSION_BUNDLE);
    //            yield return bundle;

    //            _assetBundle = bundle.assetBundle;


    //            if (_assetBundle != null)
    //            {
    //                lstAssetBundle.Add(_assetBundle);

    //                //_assetBundle.Unload(false);
    //                bundle.Dispose();
    //                _number += 1;
    //                float percent = (float)_number / (float)maxLink;
    //                //slProcess.value = percent;
    //                txtProcess.text = (int)(percent * 100) + " / 100";
    //            }
    //            else
    //            {
    //                Debug.Log("Bundle load null."); yield break;
    //            }
    //        }
    //        /* END Load all AssetsBundle temp */

    //        StartCoroutine(Helper.LoadSceneAsync(MoveScene.MAP_GAME, txtPercent));

    //        #region
    //        ///* Load prefab bundle */        
    //        //WWW _wwwPrefab = WWW.LoadFromCacheOrDownload(URL_PREFAB, VERSION_BUNDLE);
    //        //yield return _wwwPrefab;
    //        //_bundlePrefab = _wwwPrefab.assetBundle;

    //        //if (_bundlePrefab != null)
    //        //{
    //        //    //foreach (string asset in _bundlePrefab.GetAllAssetNames())
    //        //    //{
    //        //    //    AssetBundleRequest request = _bundlePrefab.LoadAssetAsync(asset, typeof(GameObject));
    //        //    //    yield return request;

    //        //    //    try
    //        //    //    {
    //        //    //        if (request != null)
    //        //    //        {
    //        //    //            GameObject obj = request.asset as GameObject;
    //        //    //            Instantiate(obj);
    //        //    //        }
    //        //    //        else { Debug.Log("Bundle request NULL"); Debug.Log(asset); yield break; }
    //        //    //    }
    //        //    //    catch { Debug.Log("CATCH bundle request null"); }
    //        //    //}

    //        //    //_bundlePrefab.Unload(false);
    //        //    _wwwPrefab.Dispose();
    //        //    //Debug.Log("Load bundle Prefab success");
    //        //}
    //        //else { Debug.Log("Bundle Prefab load null."); yield break; }
    //        ///* END Load prefab bundle */

    //        ///* Load scene bundle */
    //        //AssetBundle _bundleScene;
    //        //WWW _wwwScene = WWW.LoadFromCacheOrDownload(URL_SCENE, VERSION_BUNDLE);
    //        //yield return _wwwScene;
    //        //_bundleScene = _wwwScene.assetBundle;

    //        //if (_bundleScene != null)
    //        //{
    //        //    string[] scenes = _bundleScene.GetAllScenePaths();
    //        //    //foreach (string s in scenes)
    //        //    //{
    //        //    //    Debug.Log(s);
    //        //    //}
    //        //    StartCoroutine(Helper.LoadSceneAsync(MoveScene.MAP_GAME, txtPercent));

    //        //    //_bundleScene.Unload(false);
    //        //    _wwwScene.Dispose();
    //        //    //Debug.Log("Load bundle scene success");
    //        //}
    //        //else { Debug.Log("Bundle Scene load null."); yield break; }
    //        ///* END Load scene bundle */
    //        ///
    //        #endregion
    //    }

}
