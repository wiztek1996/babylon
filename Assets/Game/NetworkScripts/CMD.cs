﻿public enum CMD
{
    GAMEVERSION = 5,
    PROVIDER_ID = 3,
    PING = -1,
    LOGIN = 0,
    CURRENT_SCREEN = 126,
    REGISTER = 1,
    CMD_PROFILE = 2, // gửi profile
    SERVER_MESSAGE = 4,
    CHECK_UPDATE_GAME = 8, // kiểm tra cập nhật thay đổi info
}