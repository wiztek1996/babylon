﻿#if NETFX_CORE || WINDOWS_PHONE
using System;
using System.Linq;
using System.Text;

public class Message
{
    public sbyte command;
    public byte[] data = new byte[0];
    public Message(sbyte command)
    {
        this.command = command;
    }
    public Message(byte[] data)
    {
        command = (sbyte)data[0];
        byte[] dataTmp = new byte[data.Length - 3];
        dataTmp = data.Skip(3).ToArray();
        this.data = dataTmp;
    }

    public void writeUTF(String content)
    {
        byte[] bytes = Encoding.UTF8.GetBytes(content);
        byte[] dataTmp = new byte[data.Length + bytes.Length + 2];
        data.CopyTo(dataTmp, 0);
        short size = (short)bytes.Length;
        byte[] sizeBytes = BitConverter.GetBytes(size);
        if (BitConverter.IsLittleEndian)
            Array.Reverse(sizeBytes);
        sizeBytes.CopyTo(dataTmp, dataTmp.Length - 2 - bytes.Length);
        bytes.CopyTo(dataTmp, dataTmp.Length - bytes.Length);
        this.data = dataTmp;

    }
    public void writeInt(int value)
    {
        byte[] bytes = BitConverter.GetBytes(value);
        if (BitConverter.IsLittleEndian)
            Array.Reverse(bytes);
        byte[] dataTmp = new byte[data.Length + bytes.Length];
        data.CopyTo(dataTmp, 0);
        bytes.CopyTo(dataTmp, dataTmp.Length - bytes.Length);
        this.data = dataTmp;
    }
    public void writeFloat(float value)
    {
        byte[] bytes = BitConverter.GetBytes(value);
        byte[] byteArray = { bytes[3], bytes[2], bytes[1], bytes[0] };
        byte[] dataTmp = new byte[data.Length + bytes.Length];
        data.CopyTo(dataTmp, 0);
        byteArray.CopyTo(dataTmp, dataTmp.Length - bytes.Length);
        this.data = dataTmp;
    }
    public void writeByte(int value)
    {
        byte[] bytes = new byte[1];
        bytes[0] = (byte)value;
        byte[] dataTmp = new byte[data.Length + 1];
        data.CopyTo(dataTmp, 0);
        bytes.CopyTo(dataTmp, dataTmp.Length - bytes.Length);
        this.data = dataTmp;
    }
    public void writeShort(short value)
    {
        byte[] bytes = BitConverter.GetBytes(value);
        if (BitConverter.IsLittleEndian)
            Array.Reverse(bytes);
        byte[] dataTmp = new byte[data.Length + bytes.Length];
        data.CopyTo(dataTmp, 0);
        bytes.CopyTo(dataTmp, dataTmp.Length - bytes.Length);
        this.data = dataTmp;
    }
    public void writeBoolean(bool value)
    {
        byte[] bytes = BitConverter.GetBytes(value);
        if (BitConverter.IsLittleEndian)
            Array.Reverse(bytes);
        byte[] dataTmp = new byte[data.Length + bytes.Length];
        data.CopyTo(dataTmp, 0);
        bytes.CopyTo(dataTmp, dataTmp.Length - bytes.Length);
        this.data = dataTmp;
    }
    public void writeLong(long value)
    {
        byte[] bytes = BitConverter.GetBytes(value);
        if (BitConverter.IsLittleEndian)
            Array.Reverse(bytes);
        byte[] dataTmp = new byte[data.Length + bytes.Length];
        data.CopyTo(dataTmp, 0);
        bytes.CopyTo(dataTmp, dataTmp.Length - bytes.Length);
        this.data = dataTmp;
    }
    public void writeUnsignedShort(ushort value)
    {
        byte[] bytes = BitConverter.GetBytes(value);
        if (BitConverter.IsLittleEndian)
            Array.Reverse(bytes);
        byte[] dataTmp = new byte[data.Length + bytes.Length];
        data.CopyTo(dataTmp, 0);
        bytes.CopyTo(dataTmp, dataTmp.Length - bytes.Length);
        data = dataTmp;
    }
    public void write(byte[] bytes, int startIndex, int endIndex)
    {
        byte[] dataTmp = new byte[data.Length + bytes.Length];
        data.CopyTo(dataTmp, 0);
        bytes.CopyTo(dataTmp, dataTmp.Length - bytes.Length);
        this.data = dataTmp;
    }
    public string readUTF()
    {
        byte[] sizebytes = new byte[2];
        byte[] dataTmp = new byte[this.data.Length - 2];
        sizebytes = this.data.Take(2).ToArray();
        dataTmp = this.data.Skip(2).ToArray();
        this.data = dataTmp;
        short size = BitConverter.ToInt16(new byte[2] { sizebytes[1], sizebytes[0] }, 0);
        byte[] dataString = new byte[size];
        dataString = this.data.Take(size).ToArray();
        dataTmp = this.data.Skip(size).ToArray();
        this.data = dataTmp;
        return Encoding.UTF8.GetString(dataString, 0, size);
    }
    public int readInt()
    {
        byte[] dataInt = new byte[4];
        dataInt = this.data.Take(4).ToArray();
        byte[] dataTmp = this.data.Skip(4).ToArray();
        this.data = dataTmp;
        return BitConverter.ToInt32(new byte[4] { dataInt[3], dataInt[2], dataInt[1], dataInt[0] }, 0);
    }
    public float readFloat()
    {
        byte[] dataFloat = new byte[4];
        dataFloat = this.data.Take(4).ToArray();
        byte[] dataTmp = this.data.Skip(4).ToArray();
        this.data = dataTmp;
        byte[] byteArray = { dataFloat[3], dataFloat[2], dataFloat[1], dataFloat[0] };
        return System.BitConverter.ToSingle(byteArray, 0);
    }
    public long readLong()
    {
        byte[] dataLong = new byte[8];
        dataLong = this.data.Take(8).ToArray();
        byte[] dataTmp = this.data.Skip(8).ToArray();
        this.data = dataTmp;
        return BitConverter.ToInt64(new byte[8] { dataLong[7], dataLong[6], dataLong[5], dataLong[4], dataLong[3], dataLong[2], dataLong[1], dataLong[0] }, 0);
    }
    public short readShort()
    {
        byte[] dataShort = new byte[2];
        dataShort = this.data.Take(2).ToArray();
        byte[] dataTmp = this.data.Skip(2).ToArray();
        this.data = dataTmp;
        return BitConverter.ToInt16(new byte[2] { dataShort[1], dataShort[0] }, 0);
    }
    public sbyte readByte()
    {
        byte[] dataByte = new byte[1];
        dataByte = this.data.Take(1).ToArray();
        byte[] dataTmp = this.data.Skip(1).ToArray();
        this.data = dataTmp;
        return (sbyte)dataByte[0];
    }
    public bool readBoolean()
    {
        byte[] dataBoolean = new byte[1];
        dataBoolean = this.data.Take(1).ToArray();
        byte[] dataTmp = this.data.Skip(1).ToArray();
        this.data = dataTmp;
        return Convert.ToBoolean(dataBoolean[0]);
    }
    public ushort readUnsignedShort()
    {
        byte[] dataShort = new byte[2];
        dataShort = data.Take(2).ToArray();
        byte[] dataTmp = data.Skip(2).ToArray();
        data = dataTmp;
        return BitConverter.ToUInt16(new byte[2] { dataShort[1], dataShort[0] }, 0);
    }
    public void read(byte[] bytes, int startIndex, int endIndex)
    {
        bytes = data.Take(endIndex).ToArray();
        byte[] dataTmp = data.Skip(endIndex).ToArray();
        this.data = dataTmp;
    }
    public override bool Equals(object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (this.GetType() != obj.GetType())
            return false;
        Message other = (Message)obj;
        return other.command == command;
    }

    public override int GetHashCode()
    {
        // Which is preferred?

        return command;

        //return this.FooId.GetHashCode();
    }
    public sbyte[] toSbyteArray(byte[] byteArray)
    {
        sbyte[] sbytes = new sbyte[byteArray.Length];
        System.Buffer.BlockCopy(byteArray, 0, sbytes, 0, byteArray.Length);
        return sbytes;
    }
    public byte[] getData()
    {
        byte[] dataTmp = new byte[data.Length + 3];
        byte[] dataCMD = new byte[1];
        dataCMD[0] = (byte)command;
        dataCMD.CopyTo(dataTmp, 0);
        short size = (short)data.Length;
        byte[] bytesSize = BitConverter.GetBytes(size);
        if (BitConverter.IsLittleEndian)
            Array.Reverse(bytesSize);
        bytesSize.CopyTo(dataTmp, 1);
        data.CopyTo(dataTmp, 3);
        return dataTmp;
    }
}
#else
using UnityEngine;
using System.IO;
using System;
public class Message
{

    public CMD command;
    //public BinaryReader dis;
    //public BinaryWriter dos;
    //public MemoryStream outStream;
    public BinaryReaderEx iss;
    public BinaryWriterIns os;
    public DataOutputStream dos = null;
    public DataInputStream dis = null;
    public MemoryStream ms = null;
    public Message(CMD command)
    {
        this.command = command;
        ms = new MemoryStream();
        os = new BinaryWriterIns(ms);
        dos = new DataOutputStream(os);
    }

    public Message(sbyte command, byte[] data)
    {
        this.command = (CMD) command;
        ms = new MemoryStream(data);
        iss = new BinaryReaderEx(ms);
        dis = new DataInputStream(iss);
    }
    public byte[] toByteArray()
    {
        short datalen = 0;
        byte[] data = null;
        byte[] bytes = null;
        byte[] byteNew = null;
        try
        {
            if (dos != null)
            {
                dos.Flush();
                data = ms.ToArray();
                datalen = (short)data.Length;
                dos.Close();
            }
            MemoryStream bos1 = new MemoryStream(datalen + 3);
            DataOutputStream dos1 = new DataOutputStream(new BinaryWriterIns(bos1));
            dos1.writeByteNew((int)command);
            dos1.writeShort(datalen);
            if (datalen > 0)
            {
                dos1.write(data);
            }
            bytes = bos1.ToArray();
            byteNew = new byte[bytes.Length - 3];
            int n = byteNew.Length;
            Array.Copy(bytes, 3, byteNew, 0, n);
            byteNew[0] = (byte)command;
            dos1.Close();
        }
        catch (IOException e)
        {
            Debug.Log(e.ToString());
        }
        return byteNew;
    }
    public override bool Equals(object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (this.GetType() != obj.GetType())
            return false;
        Message other = (Message)obj;
        return other.command == command;
    }

    public override int GetHashCode()
    {
        // Which is preferred?

        return (int) command;

        //return this.FooId.GetHashCode();
    }
    public string readUTF()
    {
        return dis.readUTF();
    }
    public int readInt()
    {
        return dis.readInt();
    }
    public float readFloat()
    {
        return dis.readFloat();
    }
    public long readLong()
    {
        return dis.readLong();
    }
    public sbyte readSByte()
    {
        return dis.readSByte();
    }
    
    public byte readByte()
    {
        return dis.readByte();
    }
    
    public short readShort()
    {
        return dis.readShort();
    }
    public bool readBoolean()
    {
        return dis.readBoolean();
    }
    public ushort readUnsignedShort()
    {
        return dis.readUnsignedShort();
    }
    public void read(byte[] bytes, int startIndex, int endIndex)
    {
        dis.read(bytes, startIndex, endIndex);
    }
    public void writeUTF(string s)
    {
        dos.writeUTF(s);
    }
    public void writeInt(int value)
    {
        dos.writeInt(value);
    }
    public void writeFloat(float value)
    {
        dos.writeFloat(value);
    }
    public void writeLong(long value)
    {
        dos.writeLong(value);
    }
    public void writeByte(int value)
    {
        dos.writeByte(value);
    }
    public void writeShort(short value)
    {
        dos.writeShort(value);
    }
    public void writeBoolean(bool value)
    {
        dos.writeBoolean(value);
    }
    public void write(byte[] bytes, int startIndex, int endIndex)
    {
        dos.write(bytes, startIndex, endIndex);
    }


}
#endif