﻿#if NETFX_CORE || WINDOWS_PHONE // || UNITY_EDITOR
using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Storage.Streams;

class NetworkUtil
{
    private SocketWP.SocketWP socket;
    public static NetworkUtil instance;
    private const int TIME_CHECK_DATA_SERVER = 14;
    public static int TIME_WAIT_READ = 70;
    private MessageHandler messageHandler;
    public NetworkUtil()
    {
    }
    public static NetworkUtil GI()
    {
        if (instance == null)
        {
            instance = new NetworkUtil();
        }
        return instance;
    }

    public bool isConnected()
    {
        if (socket == null)
            return false;
        return socket.connected;
    }
    public void connectToServer(Message msg)
    {
        if (socket == null)
        {
            Debug.WriteLine("create connect ");
            socket = new SocketWP.SocketWP();
            socket.createConnect(Res.IP, Res.PORT + "", msg == null ? null : msg.getData());
            startThread();
        }
        else
        {
            Debug.WriteLine("send connect ");
            socket.sendMessage(msg == null ? null : msg.getData());
        }
    }
    private async void startThread()
    {
        Task m1task = thread1();
        Task all = Task.WhenAll(m1task);
        await all;
    }
    private async Task thread1()
    {
        await Task.Run(async () =>
        {
            while (!isConnected())
            {
                Debug.WriteLine("try read message ");
                await Task.Delay(100);
            }
            while (isConnected())
            {
                IBuffer buffer = new byte[64000].AsBuffer();
                await socket.socket.InputStream.ReadAsync(buffer, buffer.Capacity, InputStreamOptions.Partial);
                byte[] data = buffer.ToArray();
                int size = data.Length;
                while (data.Length > 0)
                {
                    byte[] cmdBytes = data.Take(1).ToArray();
                    data = data.Skip(1).ToArray();
                    byte[] sizeBytes = data.Take(2).ToArray();
                    data = data.Skip(2).ToArray();
                    short sizeData = BitConverter.ToInt16(new byte[2] { sizeBytes[1], sizeBytes[0] }, 0);
                    byte[] dataBytes = data.Take(sizeData).ToArray();
                    data = data.Skip(sizeData).ToArray();
                    byte[] result = new byte[dataBytes.Length + 3];
                    cmdBytes.CopyTo(result, 0);
                    sizeBytes.CopyTo(result, 1);
                    dataBytes.CopyTo(result, 3);
                    sbyte[] sbytes = new sbyte[result.Length];
                    System.Buffer.BlockCopy(result, 0, sbytes, 0, result.Length);
                    Message msg = new Message(result);
                    Debug.WriteLine("Read " + msg.command);
                    messageHandler.processMessage(msg);
                }
                await Task.Delay(100);
            }
            if (messageHandler != null)
            {
                messageHandler.onDisconnected();
            }
            close();
        });

    }
    public void sendMessage(Message msg)
    {
        if (isConnected())
        {
            socket.sendMessage(msg.getData());
            Debug.WriteLine("Send " + msg.command);
        }
    }
    public void close()
    {
        cleanNetwork();
    }
    public void cleanNetwork()
    {
        B.Instance.avatarFB = null;
        if (B.Instance.Scene == 0)
        {
            LoginController.Instance.dialogDisconnected.GetComponent<UnityEngine.Animator>().enabled = false;
            LoginController.Instance.dialogDisconnected.SetActive(true);
        }
        else if (B.Instance.Scene == 1)
        {
            UIMapGame.Instance.dialogDisconnected.GetComponent<UnityEngine.Animator>().enabled = false;
            UIMapGame.Instance.dialogDisconnected.SetActive(true);
        }//NgatKetNoi();
        if (socket != null)
            socket.closeConnect();
    }
    //private async void NgatKetNoi()
    //{
    //    await Task.Delay(3000);
    //    if (socket != null)
    //        socket.closeConnect();
    //}
    public void resume(bool pausestatus)
    {

    }
    public void registerHandler(MessageHandler messageHandler)
    {
        this.messageHandler = messageHandler;
    }
}
#else
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

/// <summary>
/// Gửi và nhận dữ liệu giữa CLIENT & SERVER
/// </summary>
public class NetworkUtil
{
    private MessageHandler messageHandler;
    private bool connected;
    private static NetworkUtil instance;
    private Message message;

    private readonly Socket _socket = null;

    private Thread connectThread;
    private Thread pingThread;
    private int maxRetry = 1;

    /// <summary>
    /// Get Instance of NetworkUtil
    /// </summary>
    /// <returns></returns>
    public static NetworkUtil GI() => instance ?? (instance = new NetworkUtil());
    public bool isConnected() =>connected;
    public void registerHandler(MessageHandler messageHandler)
    {
        this.messageHandler = messageHandler;
    }

    /// <summary>
    /// Create connect to server
    /// </summary>
    public void connectToServer(Message message)
    {
        if (!connected)
        {
            //connect to server
            if (connectThread != null)
            {
                if (connectThread.IsAlive)
                {
                    return;
                }
            }

            connectThread = new Thread(new ThreadStart(runConnect));
            connectThread.Start();
            this.message = message;
        }
        else
        {
            if (message != null)
            {
                sendMessage(message);
            }
        }
    }

    private Stream stream;
    private TcpClient client;

    /// <summary>
    /// Tạo kết nối tới server
    /// </summary>
    public void runConnect()
    {
        try
        {
            client = new TcpClient
            {
                LingerState = new LingerOption(true, 2), ReceiveBufferSize = 200000, NoDelay = true
            };
            //client.SendTimeout = 0;
            try
            {
                client.Connect(Res.IP, Res.PORT);
                isHead = true;
                _Connect();
            }
            catch (Exception e)
            {
                Debug.LogError("Connect server failed"+e);
            }
            if (connected)
            {
                stream = client.GetStream(); 
                Debug.LogWarning("Connected server successfull !!! ");
                stream.WriteTimeout = 2;
                connectThread = new Thread(threadReceiveMSG);
                connectThread.Start();
                if (message != null && !B.Instance.IsLogin) sendMessage(message);
                pingThread = new Thread(ping);
                pingThread.Start();
            }
            else
            {
                messageHandler?.onDisconnected();
                Debug.LogError("Connect server failure");
                close();
            }
        }
        catch (Exception ex)
        {
            Debug.Log("Unable to connect to internet!" + ex);
            close();
        }
    }

    /// <summary>
    /// Đợi kết nối từ server trả về
    /// </summary>
    private void _Connect()
    {
        Debug.Log($"========try connect=========== : {Res.IP} : {Res.PORT}");
        while (!connected)
        {
            if (!connected && maxRetry < 100)
            {
                maxRetry++;
                connected = client.Connected;
                Thread.Sleep(100);
            }
            else break;
        }
    }

    public const int TIME_PING = 20000;

    /// <summary>
    /// Ping để keep connect to server
    /// </summary>
    public void ping()
    {
        while (connected)
        {
            Thread.Sleep(TIME_PING);
            Message msg = new Message(CMD.PING);
            msg.writeByte((byte) 1);
            if (connected)
            {
                sendMessage(msg);
            }
        }
    }

    private const int TIME_CHECK_DATA_SERVER = 100;
    public static int TIME_WAIT_READ = 2;
    private const int sizeControl = 3;
    byte[] lengthControl = null;
    private const int ZERO = 0;

    private bool isHead;
    private int lengthData;
    private sbyte command = -1;

    public void threadReceiveMSG()
    {
        stream = client.GetStream();
        while (connected)
        {
            if (client.Available > 0)
            {
                try
                {
                    if (isHead)
                    {
                        if (client.Available < sizeControl)
                        {
                            if (!connected)
                            {
                                break;
                            }

                            continue;
                        }

                        lengthControl = new byte[sizeControl];
                        stream.Read(lengthControl, ZERO, sizeControl);
                        command = (sbyte) lengthControl[0];
                        byte[] lengthMessage = new byte[sizeControl - 1];
                        lengthMessage[0] = lengthControl[1];
                        lengthMessage[1] = lengthControl[2];
                        lengthData = BytesToInt(lengthMessage, ZERO);
#if UNITY_EDITOR
                        if (lengthData > client.ReceiveBufferSize)
                        {
                            Debug.LogWarning("Buffer size receive Length very big: " + lengthData);
                            cleanNetwork();
                        }
#endif
                        isHead = false;
                    }

                    if (!isHead)
                    {
                        if (client.Available < lengthData)
                        {
                            Thread.Sleep(5); // Debug.LogWarning("WAIT: " + client.Available);
                            continue;
                        }

                        if (lengthData == ZERO)
                        {
                            ProcessMessageFromData(command, new byte[0]);
                        }
                        else
                        {
                            byte[] msgByte = new byte[lengthData];
                            stream.Read(msgByte, ZERO, lengthData);
                            /*new Thread(new ThreadStart(delegate { */
                            ProcessMessageFromData(command, msgByte); /* })).Start();*/
                        }

                        isHead = true;
                        lengthData = ZERO;
                    }
                }
                catch (Exception e)
                {
#if UNITY_EDITOR
                    Debug.Log(e);
#endif
                    if (messageHandler != null)
                    {
                        messageHandler.onDisconnected();
                    }

                    cleanNetwork();
                }
            }
            else
            {
                bool sConnected = true;
                if (client.Client.Poll(0, SelectMode.SelectRead))
                {
                    if (!client.Connected)
                        sConnected = false;
                    else
                    {
                        byte[] b = new byte[1];
                        try
                        {
                            if (client.Client.Receive(b, SocketFlags.Peek) == 0)
                            {
                                // Client disconnected
                                sConnected = false;
                            }
                        }
                        catch
                        {
                            sConnected = false;
                        }
                    }
                }

                if (!sConnected)
                {
                    // Client disconnected
                    if (messageHandler != null)
                    {
                        messageHandler.onDisconnected();
                    }

                    cleanNetwork();
                }

                Thread.Sleep(TIME_CHECK_DATA_SERVER);
            }
        }
    }

    private static int BytesToInt(byte[] data, int offset)
    {
        int num = 0;
        for (int i = offset; i < offset + 2; i++)
        {
            num <<= 8;
            num |= (data[i] & 0xff);
        }

        return num;
    }

    private void ProcessMessageFromData(sbyte commands, byte[] data)
    {
        try
        {
#if UNITY_EDITOR
            if (commands != (int) CMD.PING)
                Debug.Log($@"<color=green> Read : {commands} [{(CMD)commands}] </color>");
#endif
            Message msg = new Message(commands, data);
            if (messageHandler == null)
            {
                ListernerServer ls = new ListernerServer();
                ls.initConnect();
            }
            messageHandler?.processMessage(msg);
            Thread.Sleep(TIME_WAIT_READ);
        }
        catch (Exception e)
        {
            Debug.LogWarning(e);
        }
    }

    private string GetBytesToString(byte[] bytes)
    {
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        string separator = " ";
        for (int i = 0; i < bytes.Length; i++)
        {
            builder.Append(bytes[i]).Append(separator);
        }

        return builder.ToString();
    }

    public void close()
    {
        Debug.Log("END Connection To Server !");
        cleanNetwork();
    }

    public void cleanNetwork()
    {
        try
        {
            B.Instance.IsLogin = false;
            Res.IP = Stats.IP;
            Res.PORT = Stats.PORT;
            connected = false;
            if (client != null)
                client.Close();
            if (_socket != null)
            {
                try
                {
                    _socket.Close();
                }
                catch (SocketException ex)
                {
                    Debug.LogException(ex);
                }
            }

            maxRetry = 1;
            connectThread = null;
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
        finally
        {
            if (connectThread != null && connectThread.IsAlive)
            {
                connectThread.Abort();
            }
        }
    }

    /// <summary>
    /// Send Message to server
    /// </summary>
    public void sendMessage(Message msg)
    {
        try
        {
            byte[] b = msg.toByteArray();
            stream.Write(b, 0, b.Length);
#if UNITY_EDITOR
            if (msg.command != CMD.PING)
                Debug.Log($@"<color=#FF00FF> Send: {(int)msg.command} [{msg.command}] </color>");
#endif
        }
        catch (Exception ex)
        {
            Debug.LogError("LOG_EXCEPTION: " + ex);
            if (messageHandler != null)
                messageHandler.onDisconnected();
            close();
        }
    }

    public void resume(bool pausestatus)
    {
    }
}
#endif