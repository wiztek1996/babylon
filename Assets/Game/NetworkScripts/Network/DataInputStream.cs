﻿using UnityEngine;
using System;
using System.IO;

public class DataInputStream
{
    private byte[] ReadBuffer = new byte[8];

    private BinaryReaderEx ClientInput;

    public DataInputStream(BinaryReaderEx _clientInput)
    {
        ClientInput = _clientInput;
    }

    public int Read(byte[] b)
    {
        return ClientInput.Read(b, 0, b.Length);
    }
    
    public void read(sbyte[] b)
    {
        int n = b.Length;
        for (int i = 0; i < n; i++)
        {
            b[i] = (sbyte)ClientInput.ReadByte();
        }
        //return ClientInput.Read(b, 0, b.Length);
    }
    public int read(byte[] b, int off, int len)
    {
        return ClientInput.Read(b, off, len);
    }

    public void readFully(byte[] b)
    {
        readFully(b, 0, b.Length);
    }

    public void readFully(byte[] b, int off, int len)
    {
        if (len < 0)
        {
            throw new IndexOutOfRangeException();
        }

        int n = 0;
        while (n < len)
        {
            int count = ClientInput.Read(b, off + n, len - n);
            if (count < 0)
            {
                throw new EndOfStreamException();
            }
            n += count;
        }
    }

    public sbyte readSByte()
    {
        return (sbyte)ClientInput.ReadByte();
    }

    public byte readByte()
    {
        return (byte) ClientInput.ReadByte();
    }

    public bool readBoolean()
    {
        return (ClientInput.ReadByte() != 0);
    }
    

    public string readUTF()
    {
        int utflen = this.readUnsignedShort();
        byte[] data = new byte[utflen];
        this.readFully(data, 0, utflen);
        return System.Text.Encoding.UTF8.GetString(data);
    }

    public double readDouble()
    {
        return BitConverter.Int64BitsToDouble(readLong());
    }

    public float readFloat()
    {
        byte b1 = ClientInput.ReadByte();
        byte b2 = ClientInput.ReadByte();
        byte b3 = ClientInput.ReadByte();
        byte b4 = ClientInput.ReadByte();
        byte[] newArray = new[] { b4, b3, b2, b1 };
        return BitConverter.ToSingle(newArray, 0);
        //return (float)readDouble();
    }

    public int readInt()
    {
        sbyte b1 = (sbyte)ClientInput.ReadByte();
        sbyte b2 = (sbyte)ClientInput.ReadByte();
        sbyte b3 = (sbyte)ClientInput.ReadByte();
        sbyte b4 = (sbyte)ClientInput.ReadByte();
        return b1 << 24 | (b2 & 0xff) << 16 | (b3 & 0xff) << 8 | (b4 & 0xff);
    }

    public long readLong()
    {
        readFully(ReadBuffer, 0, 8);
        return (((long)ReadBuffer[0] << 56) +
                ((long)(ReadBuffer[1] & 255) << 48) +
                ((long)(ReadBuffer[2] & 255) << 40) +
                ((long)(ReadBuffer[3] & 255) << 32) +
                ((long)(ReadBuffer[4] & 255) << 24) +
                ((ReadBuffer[5] & 255) << 16) +
                ((ReadBuffer[6] & 255) << 8) +
                ((ReadBuffer[7] & 255) << 0));
    }

    public short readShort()
    {
        sbyte a1 = (sbyte)ClientInput.ReadByte();
        sbyte a2 = (sbyte)ClientInput.ReadByte();
        return (short)(((a1 & 0xff) << 8) | (a2 & 0xff));
    }

    public ushort readUnsignedShort()
    {
        sbyte a1 = (sbyte)ClientInput.ReadByte();
        sbyte a2 = (sbyte)ClientInput.ReadByte();
        return (ushort)(((a1 & 0xff) << 8) | (a2 & 0xff));
    }
    public void Close()
    {
        ClientInput.Close();
    }
}