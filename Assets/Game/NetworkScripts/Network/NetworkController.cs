﻿using System;
using UnityEngine;

public class NetworkController : MonoBehaviour
{
    private static NetworkController instance;

    private void Awake()
    {
        if (instance == null)
        {
            //If I am the first instance, make me the Singleton
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != instance) Destroy(gameObject);
        }
    }
    private void OnApplicationQuit()
    {
        NetworkUtil.GI().cleanNetwork();
    }
    private void OnApplicationPause(bool pause)
    {
        NetworkUtil.GI().resume(pause);
    }
}
