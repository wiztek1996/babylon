﻿using UnityEngine;
public abstract class MessageHandler
{

    protected abstract void serviceMessage(Message message);

    public MessageHandler()
    {
    }

    public void processMessage(Message message)
    {
        try
        {
            serviceMessage(message);
        }
        catch (System.Exception ex)
        {
            Debug.LogException(ex);
        }
    }

    public abstract void onConnectionFail();

    public abstract void onDisconnected();

    public abstract void onConnectOk();
}
