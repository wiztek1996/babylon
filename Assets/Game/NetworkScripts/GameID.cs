﻿public class GameID {
    public const int PHOM = 0;
    public const int TLMN = 1;
    public const int MAU_BINH = 3;
    public const int POKERMINI = 8;
    public const int SAM = 6;
    public const int CHUONG = 7;
    public const int RESET = -1;
    public const int XOC_DIA = 9;
    public const int TAIXIU = 10;
    public const int TLMNSL = 11;
    public const int BAU_CUA = 15; 

    public const byte SLOT_COTRUYEN = 21;  // SLOT_COTRUYEN
    public const byte SLOT_VQHP = 22;    // SLOT_VQHP
    public const int SLOT_SIEUXE = 3;  // có thể bỏ sau
    
    // Sắp ra
    public const int BA_CAY = 4;
    public const int CHAN = 13;
    public const int LIENG = 5;
    public const int POKER = 14;
    public const int XITO = 2;
    
}
