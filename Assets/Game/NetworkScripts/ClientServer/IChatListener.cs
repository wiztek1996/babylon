public interface IChatListener
{
    void onLoginFail();
    void onLoginSuccess(Message msg);
    void onDisConnect();

    /// <summary>
    /// Get command from SERVER
    /// </summary>
    /// <param name="messageId"></param>
    void ProcessAllCommand(Message msg);
  
}