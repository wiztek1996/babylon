﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Send Request & data to SERVER
/// </summary>
public static class SendData
{

    private static void SendMessage(Message msg) => NetworkUtil.GI().sendMessage(msg); 
    public static Message OnRequestListServers()
    {
        Message msg = new Message(CMD.LOGIN);
        return msg;
    }
    
    public static void OnLogin(string username ,string password,LoginType type)
    {
        Message msg = new Message(CMD.LOGIN);
        msg.writeUTF(username);
        msg.writeUTF(password);
        msg.writeByte((int)type);
        msg.writeUTF(AgentUnity.GetIMEI());
        PlayerPrefs.SetString(Stats.USERNAME,username);
        PlayerPrefs.SetString(Stats.PASSWORD,password);
        SendMessage(msg);
    }
    
    public static void OnRegister(string username ,string password,sbyte providerId = (int)CMD.PROVIDER_ID)
    {
        Message msg = new Message(CMD.REGISTER);
        msg.writeUTF(username);
        msg.writeUTF(password);
        msg.writeByte(providerId);
        msg.writeUTF(AgentUnity.GetIMEI());
        SendMessage(msg);
    }
    
    public static void OnCheckUpdateGame()
    {
        Message msg = new Message(CMD.CHECK_UPDATE_GAME);
        msg.writeByte((int)CMD.PROVIDER_ID);
        msg.writeInt((int)CMD.GAMEVERSION);
        SendMessage(msg);
    }
    
    
    public static void OnCurrentScreen(ScreenData screenData)
    {
        Message msg = new Message(CMD.CURRENT_SCREEN);
        msg.writeByte((int)screenData);
        SendMessage(msg);
    }
}