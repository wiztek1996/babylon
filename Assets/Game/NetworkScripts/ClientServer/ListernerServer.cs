﻿using System;
using System.Collections.Generic;
using System.IO;
using DemoObserver;
using UnityEngine;


/// <summary>
/// Xử lý response from SERVER gửi về
/// </summary>
public class ListernerServer : IChatListener
{
    private IChatListener _chatListenerImplementation;

    public void initConnect()
    {
        NetworkUtil.GI().registerHandler(ProcessHandler.getInstance());
        ProcessHandler.setListenner(this);
    }

    /// <summary>
    /// Khởi tạo lắng nghe sự kiện từ server
    /// </summary>
    public ListernerServer()
    {
        // TODO Auto-generated constructor stub
    }

    public void onDisConnect()
    {
        Debug.Log("DISCONNECTED BY SERVER !!!");
        NetworkUtil.GI().close();
    }


    private void ShowMessage(string content) => Service.MESSAGE_DIALOG.Show(content);

    /// <summary>
    /// Process Login success
    /// </summary>
    public void onLoginSuccess(Message msg)
    {
        new CommandLoginSystem(msg);
    }

    public void onLoginFail()
    {
        ShowMessage("Login Failed !");
    }


    public void ProcessAllCommand(Message msg)
    {
        switch (msg.command)
        {
            #region Already process in ProcessHandler
            case CMD.CURRENT_SCREEN:
                break;
            case CMD.PING:
                Debug.Log("STAY CONNECTED TO SERVER");
                break;
            case CMD.GAMEVERSION:
                break;
            case CMD.PROVIDER_ID:
                break;
            case CMD.LOGIN: //
                break;
            #endregion
            
            case CMD.REGISTER:
                ShowMessage(msg.readBoolean() ? 
                    "Register Successfull ,Please login !" 
                    : 
                    "Register Failed !");
                break;

            case CMD.SERVER_MESSAGE: ShowMessage(msg.readUTF());
                break;
            case CMD.CHECK_UPDATE_GAME: new CommandCheckUpdateSystem(msg);
                break;
            case CMD.CMD_PROFILE: new CommandProfileSystem(msg);
                break;
            default:
                Debug.LogError("CMD: " + msg.command + " not Listened !!!");
                break;
        }
    }
}