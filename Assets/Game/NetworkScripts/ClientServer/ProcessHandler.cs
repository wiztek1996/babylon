﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
/// <summary>
/// Xử lý nhận RESPONSE from SERVER
/// </summary>
public class ProcessHandler : MessageHandler
{
    private static ProcessHandler instance;

    //int send = 0;
    static int step;
    public static ProcessHandler getInstance()
    {
        if (instance == null)
        {
            instance = new ProcessHandler();
        }

        return instance;
    }

    public ProcessHandler()
    {
        
    }

    public override void onConnectionFail()
    {
        throw new NotImplementedException();
    }

    public override void onConnectOk()
    {
        Debug.Log("Connect OK...");
    }

    public override void onDisconnected()
    {
        DoOnMainThread.ExecuteOnMainThread.Enqueue(() => { listenner.onDisConnect(); });
    }

    protected override void serviceMessage(Message message)
    {
        try
        {
            DoOnMainThread.ExecuteOnMainThread.Enqueue(() =>
            {
                switch (message.command)
                {
                    case CMD.LOGIN:
                        try
                        {
                            if (B.Instance.IsLogin)
                            {
                                bool success = message.readBoolean();
                                if (success) {
                                    listenner.onLoginSuccess(message);
                                } else {
                                    listenner.onLoginFail();
                                }
                            }
                            else
                            {
                                List<ServerInfo> lstServer = new List<ServerInfo>();
                                byte serverInfos = message.readByte();
                                for (int i = 0; i < serverInfos; i++)
                                {
                                    lstServer.Add(new ServerInfo
                                    {
                                        id = message.readSByte(),
                                        name = message.readUTF(),
                                        isUnLock = message.readBoolean(),
                                        ip = message.readUTF(),
                                        port = message.readShort(),
                                    });
                                }
                                Service.SERVER_SELECT.InitListServer(lstServer);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            throw;
                        }

                        break;

                    default:
                        listenner.ProcessAllCommand(message);
                        secondHandler?.processMessage(message);
                        break;
                }
            });
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    private static IChatListener listenner;

    public static void setListenner(ListernerServer listener)
    {
        listenner = listener;
    }

    private static MessageHandler secondHandler;

    public void setSecondHandler(MessageHandler handler)
    {
        secondHandler = null;
        secondHandler = handler;
    }
}